#ifndef MC145000_H
#define MC145000_H
// --------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------
// 			MC145000  LCD module Driver
// --------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------
// Mark Giebler
#include <stdint.h> // for uint8_t
#include <binary.h>	// arduino header for the B00000001 and such

// prototypes for my MC145000 LCD driver.
void LCDsetup(int clk, int data, int sync);
void LCDprint(int number);
void LCDsetDigit(uint8_t index, uint8_t bits);
void LCDupdate();


/* **********************************************************
 * Define bit patterns for the salvaged LCD display segments 
 * and build numbers from the patterns
 * Hex numbers
 * **********************************************************/
#define Sa B00000001
#define Sb B00000010
#define Sc B00000100
#define Sd B10000000
#define Se B01000000
#define Sf B00010000
#define Sg B00100000
#define SdecPoint B00001000

#define LCD_BLANK (B00000000)
#define LCD_ALL_ON (0xff)

#define LCD_0 (Sa|Sb|Sc|Sd|Se|Sf)
#define LCD_1 (Sb|Sc)
#define LCD_2 (Sa|Sb|Sg|Se|Sd)
#define LCD_3 (Sa|Sb|Sc|Sd|Sg)
#define LCD_4 (Sb|Sc|Sf|Sg)
#define LCD_5 (Sa|Sf|Sg|Sc|Sd)
#define LCD_6 (Sf|Sg|Sc|Sd|Se)
#define LCD_7 (Sa|LCD_1)
#define LCD_8 (Sg|LCD_0)
#define LCD_9 (Sf|Sg|LCD_7)
#define LCD_A (Se|Sf|Sg|Sa|Sb|Sc)
#define LCD_B (Sf|Se|Sd|Sc|Sg)
#define LCD_C (Sg|Sc|Sd)
#define LCD_D (Sb|Sc|Sd|Se|Sg)
#define LCD_E (Sa|Sf|Sg|Se|Sd)
#define LCD_F (Sa|Sf|Sg|Se)
#define LCD_H (Sf|Sg|Se|Sb|Sc)
#define LCD_L (Sf|Se|Sd)
#define LCD_MINUS (Sg)

// Define some indicator bits
// Right side 
#define LCD_IND__degF	B00000001	
#define LCD_IND__T		B00000010	/* Both left and right sides */
#define LCD_IND__EQU	B00000100
#define LCD_IND__H		B00001000
#define LCD_IND__degC	B00010000
#define LCD_IND__DELTA	B00100000
#define LCD_IND__M		B01000000
#define LCD_IND__Pmin	B10000000	/* P- */
// Left side
#define LCD_IND__K		B00000001
//#define LCD_IND__T		B00000010
#define LCD_IND__Gmin   B00000100	/* G- */
#define LCD_IND__TYPE   B00010000
#define LCD_IND__J      B00100000
#define LCD_IND__LoBatt B10000000

#endif
