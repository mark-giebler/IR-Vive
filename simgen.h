#ifndef SIMGEN_H
#define SIMGEN_H

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

// uncomment to enable IR pulse simulator
#define ENABLE_SIM_GEN

#define SIM_S1S2_DELTA_H	(113ul)		//uSec offset between S1 and S2 simulated signals for horizontal frame.
#define SIM_S1S2_DELTA_V	(20ul)		//uSec offset between S1 and S2 simulated signals for vertical frame.

#define SIM_HORIZ_TIME		(2880ul)	//uSec time between sync pulse and horizontal pulse
#define SIM_VERT_TIME		(4660ul)	//uSec time between sync pulse and vertical pulse

void init_simgen_engine(int simpin1, int simpin2);
int simsrc_generator(int simpin1, int simpin2);

#ifdef __cplusplus
} // extern "C"
#endif

#endif
