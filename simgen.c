// =============================================================================
// put the simulator generator function in a C file such that
// we can conveniently use an enum to define the state machine states and
// easily increment through the states as we generate the simulated pulse stream.
//
// =============================================================================
#include <Arduino.h>
#include "IR_Vive.h"
#include "simgen.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

typedef enum st_simgen
{
	// enumerate the states in the order in which the pattern is generated.
	st_sync1_on = 0,
	st_sync1_off,
	st_sync2_on,
	st_sync2_off,
	st_sync3_on,
	st_sync3_off,
	st_horiz_on,
	st_horiz_off,
	st_sync4_on,
	st_sync4_off,
	st_vert_on,
	st_vert_off,
	st_sequence_done,
}st_simgen_t;

st_simgen_t simsrc1_state = st_sequence_done;
static 	uint32_t simsrc1_micros = 0;
st_simgen_t simsrc2_state = st_sequence_done;
static 	uint32_t simsrc2_micros = 0;

// prototype
void simsrc_pin_control(uint32_t now, int simpin, st_simgen_t *simsrc_state, uint32_t* simsrc_micros, int sensor);

// =============================================================================
// Simulated IR signal generator
// for testing without a light house.
// This function must be called periodically (often) from within the loop()
//
// Generates a simulated IR signal pattern on the requested output pin (simpinX).
// The simpinXs must be configured by the caller as a digital output.
// On the hardware platform, the output pins simpin1 & simpin2 would be connected
// to the sensor input pins S1 & S2 respectively. (remove the sensors)
//
// The pattern on the simpinXs:
//	Two framing sync frames,
//	A Horizontal pulse 2083 uSec after start of frame (simulates sensors to the left of center)
//	A Vertical pulse 4666 uSec after start of frame (simulates sensor below the center)
//	Rinse and repeat...
//
//   _                _                _                   _                  _
//  | |              | |              | |      |          | |            |   | |
//  | |              | |              | |      |          | |            |   | |
// -- ---------------- ---------------- ------- ----------- ------------- ---- -----
//  +---- 8.3 Ms ----+---- 8.3 Ms ----+--- Th -+          +------ Tv ----+
//      Sync Field        Sync Field      Horiz. Field        Vert. Field        SYNC .. etc
//
// The simulated S1 and S2 signals are skewed from each other during horizontal
// and vertical pulse generation to simulate the sweeping beams.
//
// Future:
// - modulate horizontal and vertical pulse positions to simulate the sensors
// moving in some pattern (a 1 meter circle)
// - allow caller to specify location of sensors to simulate.
//
// =============================================================================
int simsrc_generator(int simpin1, int simpin2)
{
	#ifdef	ENABLE_SIM_GEN
	uint32_t now = micros();
	// check if excessive time between calls
	if( now > ((simsrc1_micros) + (MAXPULSE/UNITS_SCALE)))
	{
		// in which case we need to re-start the state machine sequence
		init_simgen_engine(simpin1,simpin2);
		return 0;
	}
	simsrc_pin_control(now, simpin1, &simsrc1_state, &simsrc1_micros, 1);
//	now = micros();
	simsrc_pin_control(now, simpin2, &simsrc2_state, &simsrc2_micros, 2);
	if(simsrc1_state == st_sequence_done)
	{
		return 1;	// complete sequence
	}
	return 0;		// sequence not done yet
	#else
	return 1;
	#endif
}

// The state machine that controls a pin.
void simsrc_pin_control(uint32_t now, int simpin, st_simgen_t *simsrc_state, uint32_t* simsrc_micros, int sensor)
{
	switch((*simsrc_state))
	{
		case st_sync1_on:			// first frame empty sync frame
		case st_sync2_on:			// second frame empty sync frame
			digitalWrite(simpin, HIGH);
			if( (*simsrc_micros) < now )
			{
				// on time ended, set next sync off time
				(*simsrc_micros) = now + (PULSE_RATE_SYNC-PULSE_SYNC_WIDTH)/UNITS_SCALE;
				(*simsrc_state)++;
			}
			break;
		case st_sync1_off:
		case st_sync2_off:
			digitalWrite(simpin, LOW);
			if( (*simsrc_micros) < now )
			{
				// off time ended, set next sync on pulse width.
				(*simsrc_micros) = now + PULSE_SYNC_WIDTH/UNITS_SCALE;
				(*simsrc_state)++;
			}
			break;
		case st_sync3_on:			// third frame starts horizontal frame
			digitalWrite(simpin, HIGH);
			if( (*simsrc_micros) < now )
			{
				// on time ended, set next sync off time before horizontal on time.
				(*simsrc_micros) = now + SIM_HORIZ_TIME-PULSE_SYNC_WIDTH/UNITS_SCALE;
				if( sensor == 2)
					// skew sensor 2 time
					(*simsrc_micros) += SIM_S1S2_DELTA_H;
				(*simsrc_state)++;
			}
			break;
		case st_sync3_off:			// start the horizontal frame.
			digitalWrite(simpin, LOW);
			if( (*simsrc_micros) < now )
			{
				// off time ended, next state starts horiz pulse on, set pulse on time.
				(*simsrc_micros) = now + PULSE_SWEEP_WIDTH/UNITS_SCALE;
				(*simsrc_state)++;
			}
			break;
		case st_horiz_on:
			digitalWrite(simpin, HIGH);
			if( (*simsrc_micros) < now )
			{
				// horiz on ended, set off time until next sync
				(*simsrc_micros) = now + (PULSE_RATE_SYNC-PULSE_SYNC_WIDTH-PULSE_SWEEP_WIDTH)/UNITS_SCALE -SIM_HORIZ_TIME;
				if( sensor == 2)
					// skew sensor 2 time
					(*simsrc_micros) -= SIM_S1S2_DELTA_H;
				(*simsrc_state)++;
			}
			break;
		case st_horiz_off:
			digitalWrite(simpin, LOW);
			if( (*simsrc_micros) < now )
			{
				// set width of next sync pulse on time.
				(*simsrc_micros) = now + PULSE_SYNC_WIDTH/UNITS_SCALE;
				(*simsrc_state)++;
			}
			break;
		case st_sync4_on:			// forth frame start vertical frame
			digitalWrite(simpin, HIGH);
			if( (*simsrc_micros) < now )
			{
				// on time ended, set next sync off time before vertical on time.
				(*simsrc_micros) = now + SIM_VERT_TIME-PULSE_SYNC_WIDTH/UNITS_SCALE;
				if( sensor == 2)
					// skew sensor 2 time
					(*simsrc_micros) += SIM_S1S2_DELTA_V;
				(*simsrc_state)++;
			}
			break;
		case st_sync4_off:			// start the vertical frame
			digitalWrite(simpin, LOW);
			if( (*simsrc_micros) < now )
			{
				// off time ended, next state starts vert pulse on, set pulse on time.
				(*simsrc_micros) = now + PULSE_SWEEP_WIDTH/UNITS_SCALE;
				(*simsrc_state)++;
			}
			break;
		case st_vert_on:
			digitalWrite(simpin, HIGH);
			if( (*simsrc_micros) < now )
			{
				// vert pulse on ended, set off time until next sync
				(*simsrc_micros) = now + (PULSE_RATE_SYNC-PULSE_SYNC_WIDTH-PULSE_SWEEP_WIDTH)/UNITS_SCALE -SIM_VERT_TIME;
				if( sensor == 2)
					// skew sensor 2 time
					(*simsrc_micros) -= SIM_S1S2_DELTA_V;
				(*simsrc_state)++;
			}
			break;
		case st_vert_off:
			digitalWrite(simpin, LOW);
			if( (*simsrc_micros) < now)
			{
				// vert pulse off ended, next state is start of sync on, set sync on pulse time.
				(*simsrc_micros) = now + PULSE_SYNC_WIDTH/UNITS_SCALE;
				// rinse and repeat the pattern
				(*simsrc_state) = st_sequence_done;
			}
			break;
		case st_sequence_done:
		default:
			(*simsrc_state) = st_sync1_on;
			(*simsrc_micros) = now + PULSE_SYNC_WIDTH/UNITS_SCALE;
			break;
	}
}


// initialize the IR simulator pin control state machines
// to generate pulse streams that are offset by the distance
// away form the lighthouse that we want to simulate.
//
// Call once from setup().
//
void init_simgen_engine(int simpin1, int simpin2)
{
	digitalWrite(simpin2, LOW);
	digitalWrite(simpin1, LOW);

	simsrc1_state = st_vert_off;
	simsrc1_micros = micros() + PULSE_SYNC_WIDTH/UNITS_SCALE;
	simsrc2_state = st_vert_off;
	simsrc2_micros = micros() + PULSE_SYNC_WIDTH/UNITS_SCALE;
}


#ifdef __cplusplus
} // extern "C"
#endif
