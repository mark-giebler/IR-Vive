#ifndef L476RG_CLOCK_H
#define L476RG_CLOCK_H
#if defined(ARDUINO_NUCLEO_L476RG)
#include "stm32yyxx_ll_cortex.h"
#include "stm32yyxx_ll_rcc.h"

#define STM32_CLOCK_NANOS	(13)	// manually coordinate with MHz setting in MyCLK_PLLconfig()

inline uint32_t nanos(void)
{
	// 77.0 MHz sysTick clock 12.99 nSec
	/* Ensure COUNTFLAG is reset by reading SysTick control and status register */
	LL_SYSTICK_IsActiveCounterFlag();
	uint32_t m = HAL_GetTick();
	uint32_t tms = SysTick->LOAD + 1;
	__IO uint32_t st = tms - SysTick->VAL;
	if (LL_SYSTICK_IsActiveCounterFlag()) {
		m = HAL_GetTick();
		st = tms - SysTick->VAL;
	}
	// scale millis to nanoseconds, and add 12.99 nSec per SysTick tick.
	return (m * 1000000ul + (st * ((uint32_t)STM32_CLOCK_NANOS)) );
}

#endif
#endif
