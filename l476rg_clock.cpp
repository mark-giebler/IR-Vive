/*
 *******************************************************************************
 * Copyright (c) 2020-2021, STMicroelectronics
 * All rights reserved.
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************

 Modify system clock frequency to get an even number for sysTimer tick rate in nano seconds.
 */

#if defined(ARDUINO_NUCLEO_L476RG)
#include "pins_arduino.h"

// uncomment to enable sysClk out on pin D7
#define ENABLE_MCO

// Define PLL config parameters for various frequencies


// Define Clock config for STM32L476GR Nucleo
// 80.0 MHz (12.50 nSec) prePLLdiv 1, PLLdiv 40, PLLdivR 2.   HSI PLL ref..
#define MCLK_80000000_HZ \
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_MSI; \
	RCC_OscInitStruct.MSIState = RCC_MSI_ON; \
	RCC_OscInitStruct.MSICalibrationValue = 0; \
	RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_6; \
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON; \
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_MSI; \
	RCC_OscInitStruct.PLL.PLLM = 1; \
	RCC_OscInitStruct.PLL.PLLN = 40; \
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7; \
	RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2; \
	RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;

// 77.0 MHz (13.99 nSec) prePLLdiv 1, PLLdiv 77, PLLdivR 4.   HSI PLL ref..
#define MCLK_77000000_HZ \
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_MSI; \
	RCC_OscInitStruct.MSIState = RCC_MSI_ON; \
	RCC_OscInitStruct.MSICalibrationValue = 0; \
	RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_6; \
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON; \
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_MSI; \
	RCC_OscInitStruct.PLL.PLLM = 1; \
	RCC_OscInitStruct.PLL.PLLN = 77; \
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7; \
	RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV4; \
	RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV4;

// select which PLL config we want. Manually coordinate nSec value in STM32_CLOCK_NANOS in l476rg_clock.h
#define MyCLK_PLLconfig() MCLK_77000000_HZ

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/**
  * @brief  System Clock Configuration
  * Replace the stm32 default SystemClock_Config()
  * Set up system clock to MyCLK_PLLconfig() settings.
  * @param  None
  * @retval None
  */
void SystemClock_Config(void)
{
	RCC_OscInitTypeDef RCC_OscInitStruct = {};
	RCC_ClkInitTypeDef RCC_ClkInitStruct = {};
	RCC_PeriphCLKInitTypeDef PeriphClkInit = {};

	/* MSI is enabled after System reset, activate PLL with MSI as source */
	MyCLK_PLLconfig();

	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
	/* Initialization Error */
		Error_Handler();
	}

	/* Initializes the CPU, AHB and APB buses clocks */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
								| RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK) {
		/* Initialization Error */
		Error_Handler();
	}

	PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USB;
	PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_PLLSAI1;
	PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_MSI;
	PeriphClkInit.PLLSAI1.PLLSAI1M = 1;
	PeriphClkInit.PLLSAI1.PLLSAI1N = 24;
	PeriphClkInit.PLLSAI1.PLLSAI1P = RCC_PLLP_DIV7;
	PeriphClkInit.PLLSAI1.PLLSAI1Q = RCC_PLLQ_DIV2;
	PeriphClkInit.PLLSAI1.PLLSAI1R = RCC_PLLR_DIV2;
	PeriphClkInit.PLLSAI1.PLLSAI1ClockOut = RCC_PLLSAI1_48M2CLK;
	if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK) {
		Error_Handler();
	}
	/* Configure the main internal regulator output voltage */
	if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK) {
		Error_Handler();
	}

	// update core freq. global variable.
	SystemCoreClockUpdate();

#ifdef ENABLE_MCO
	HAL_RCC_MCOConfig(RCC_MCO1, RCC_MCO1SOURCE_SYSCLK, RCC_MCODIV_1);
#endif
}

#ifdef __cplusplus
} // extern "C"
#endif

#endif /* ARDUINO_NUCLEO_L476RG */
