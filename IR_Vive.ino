// =================================================================
// =================================================================
//
// IR Sensor array hardware for measuring 3D location relative to an
// HTC Vive VR lighthouse.
// With better than 1 mm resolution.
//
// Eastan Giebler, Lucas Giebler, Mark Giebler
//
//
// Proto 1:
// Select Board: chipKit -> FubarinoSD (v1.5)
// Proto 2:
// Alt Board: STM32 boards -> Nucleo-64   Tools Submenu: Board part number -> Nucleo G474RE
// Alt Board: STM32 boards -> Nucleo-64   Tools Submenu: Board part number -> Nucleo L476RG
//
//
// An HTC Vive VR lighthouse emits three IR beams that are used to to determine the
// location of a device in 3 dimensions relative to the lighthouse.
// One beam is a wide conical shaped beam, the other two are scanning fan shaped laser beams.
// One fan shaped beam scans horizontally, the other vertically.
//
// Vive lighthouse wide beam and scanning lasers create a pulse stream from an IR detector:
//   _                _                _                   _                  _
//  | |              | |              | |      |          | |            |   | |
//  | |              | |              | |      |          | |            |   | |
// -- ---------------- ---------------- ------- ----------- ------------- ---- -----
//  +---- 8.3 Ms ----+---- 8.3 Ms ----+--- Th -+          +------ Tv ----+
//      Sync Field        Sync Field      Horiz. Field        Vert. Field        SYNC .. etc
//
//  The wide cone shaped beam creates 70 uSec wide pulses every 8.3333 mSec or 120 Hz. (Frame time)
//  The horizontal and vertical scanning fan shaped beams create pulses that are narrower
//  and fall between the 8.3 mSec Frame time pulses.
//  The position of these narrow pulses with respect to the wide pulses, shown as times Th & Tv,
//  will vary as an IR sensor moves horizontally and vertically with respect to the lighthouse.
//  After using raw mode for measuring; the pattern was determined to be:
//			sync, sync, Horizontal scan, Vertical scan, sync, sync, H, V, ... etc
//  	Vertical and horizontal scans happen every 4 frame times,
//  	Sweep speed seems to indicate a 3600 RPM motor (60Hz)
//
// ------------------------------------------------------------------
// Distance measure hardware info:
//  IR Sensors used:  two Fairchild QSE156
//  Two IR Sensors are spaced 5.2" (13.2 cm) apart on a solderless bread board.
//  (measured from middle pin of sensor 1 to middle pin of sensor 2)
//  The two sensor are facing the same direction, toward the lighthouse.
//  Used a dowel attached to the breadboard as a "calibrated" distance.
//  The dowel is perpendicular to the plane of the two sensors.
//  The dowel resulted in a distance of 245 mm from the plane of the two sensors to the front of the lighthouse.
//  We made a guesstimate that the center of the laser scanner's axis was 25 mm back
//  from the front of the lighthouse.
//  Therefore, our "calibrated" distance from the sensors to the center of the scanner is 270 mm.
//   @ 270 mm distance this is 27.5 degrees of sweep from one IR sensor to the next.
//  Todo: Alternate config to try: space 5.7" (14.5 cm) apart, then @ 270 mm Dx we will get a 30 degree sweep.
//
//  Proto 2: S1 to S2 spacing changed to 2.6"  (6.6 cm) and 13.75 degree sweep.
//
//  Horizontal Laser beam scans (sweeps) left to right when facing lighthouse.
//  Left IR Sensor:  Sensor 1 (S1)  - PIN_INT1
//  Right IR Sensor: Sensor 2 (S2)  - PIN_INT2
//
//  ****  S1 to S2 distance: 13.2 cm: (27.5 degree sweep) @ 270mm when plane of sensors is perpendicular to lighthouse.
//  ****  Actual S1 to S2 time delta measured was 1275 uSec @ 270 mm ==> calib_S1S2Time
//        giving time of one revolution as:  46.36 uSec/deg  * 360 deg is 16708 uSec,
//        close to 2x 8333.3 uSec that we expected.
//
//	S1
//	|
//	| h
//	|
//	+ - - - - - - Dx - - - - - [LightHouse]
//	|
//	| h
//	|
//	S2
//		In the figure above:
//			S1 and S2 are the QSE156 IR sensors.
//			2h is the distance between the sensors: 5.2" (13.208 cm) (On proto 2 changed to 6.6 cm)
//			Dx is the distance between the sensor plane and the lighthouse (a.k.a the radius)
//			Angle between h and Dx assumed to be 90 degrees.
//
//	The laser scanners rotate 180 degrees during the time between sync pulses
//	thus they take 2 times the sync pulse time to make one rotation.
//	Assuming 1 rev of laser scanner is 2 times the sync pulse time we get:
//	2 x 8333.333 is	16666.666 uSec per rev: from this we get:
//	46.296 uSec per degree ==> uSecPerDegree or 0.0216 degrees/uSec
//	For our 27.5 degree sensor spacing: calib_S1S2Time would be 1273 uSec.
//	This matches closely to actual measurements.
//	Our guesstimate on the set-back of the laser scanner axis to front of lighthouse is one source of error.
//
//	To verify/calibrate our sensor platform:
//	1. Space sensors 24.5 cm away from lighthouse face ==> calib_Dx
//		(~ 27.0 cm from center of laser spinners), at this distance the
//		laser beam will sweep 27.5 degrees between each sensor.
//	2. measure uSec delta between pulses on each sensor ==> S1S2_deltaTime
//		This should result in pulse time delta between sensors of: 27.5*46.3 = 1273 uSec ==> calib_S1S2Time.
//
//	MATH: Determining distance (Dx) from lighthouse:
//	------------------------------------------
//	Linear approximation method:
//		(assume sin() of angles less than 30 degrees is linear)
//	Distance (Dx) of S1 S2 sensor plane from lighthouse will then be:
//		Dx = ( calib_S1S2Time / S1S2_deltaTime ) * calib_Dx  (270 mm)
//
//		1 m away the sweep angle is 7.55 degrees, and should result in 350.4 uSec pulse deltaTime.
//		  - matches what we measured with the o'scope @ 1 meter.
//
//	Or using trig:
//		(𝑥,𝑦)=(𝑟 cos 𝜑, 𝑟 sin 𝜑). 𝑟 is Dx in our system.
//		Degree of the sweep between S1 and S2 equals  S1S2_deltaTime / uSecPerDegree = arcDegrees
//		h == (distance between sensors) / 2   which is 6.6 cm
//		Let C be the Hypotenuse of the triangle:
//		C = h / Sin(arcDegrees/2)
//		C^2 = Dx^2 + h^2	Three Sides of triangle.
//		Dx is the distance from the lighthouse to a perpendicular
//		line between the sensors. (Dx is what we want to display)
//		Dx^2 = C^2 - h^2
//		Dx = SQRT((6.6 / sin(arcDegrees/2))^2 - 6.6^2).
//					arcDegrees = S1S2_deltaTime / uSecPerDegree
//
//  Using trig method versus Linear approximation:
//		At ~27 cm away, sweep angle is 27.5 degrees, using that in trig math give distance of 26.977cm
//		Using linear method, we get 27.0 cm.
//		The error of the linear method gets less and less the farther away our sensors are
//		from the lighthouse.
//		Because the sin() function for small angles is closer to linear.
//  	So trig is overkill here.
//
//	Trig References:
//		Interactive sin() function where you can see the slope of the tangent line
//		for angles close to 0 doesn't change much:
//		http://www.intmath.com/differentiation-transcendental/differentiation-trigonometric-functions-interactive-applet.php
//		Interactive unit circle:
//		https://www.mathsisfun.com/algebra/trig-interactive-unit-circle.html
//
//
// -----BEGIN CI Meta Tag Data-----
//
// Info on CI Meta Tags: https://gitlab.com/mark-giebler/test__arduino_ci
//
// Declare the target hardware for auto target selection CI code:
// Target_FQBN==STMicroelectronics:stm32:Nucleo_64:pnum=NUCLEO_G474RE
//
// BSP download URL for auto target selection CI code:
// Target_BSP==https://github.com/stm32duino/BoardManagerFiles/raw/main/package_stmicroelectronics_index.json
//
// List any Arduino Native libraries required by the sketch:
// Libraries_NATIVE==Adafruit ILI9341,Adafruit GFX Library,Adafruit BusIO
//
// List any 3rd-Party Libraries to install from git repos:
// Libraries_URL==
//
// -----END CI Meta Tag Data-----
//
//
// Note: Built and worked correctly using versions 2.2.0, 2.3.0, 2.4.0, 2.7.1 of STMicroelectronics BSP with Arduino IDE 1.8.16
// Note: That Since 2.8.0 of STM32 BSP, Arduino IDE 1.8.x is no longer supported. Only Arduino IDE 2.x.
//		BSP v2.8.1 does work with arduino-cli  Version: 0.34.2 Commit: 963c1a76 Date: 2023-09-11T10:05:42Z
//
// Alternate target board FQBN: STMicroelectronics:stm32:Nucleo_64:pnum=NUCLEO_G474RE
// Alternate target board FQBN: STMicroelectronics:stm32:Nucleo_64:pnum=NUCLEO_L476RG
// Alternate target board FQBN: chipKIT:pic32:fubarino_sd:Version=v15
//
// Alternate target BSP: https://github.com/stm32duino/BoardManagerFiles/raw/main/package_stmicroelectronics_index.json
// Alternate target BSP: https://github.com/chipKIT32/chipKIT-core/raw/master/package_chipkit_index.json
//
// Note: To get programming Nucleo boards via SWD to work on Linux, we had to make sym-links to ST's bin utils:
// 		sudo ln -s /usr/local/STMicroelectronics/STM32Cube/STM32CubeProgrammer/bin/STM32_Programmer.sh /usr/local/bin/STM32_Programmer.sh
// 		sudo ln -s /usr/local/STMicroelectronics/STM32Cube/STM32CubeProgrammer/bin/STM32_Programmer_CLI /usr/local/bin/STM32_Programmer_CLI
//
//
// ------------------------------------------------------------------
//
// Modified Libraries are only needed for the Fubarino SD PIC32 board:
// Libraries: Our Modified libs to fix pic32 gcc compile errors:
//				Adafruit_GFX_Library (missing min()/max() definitions in pic32 build, and broken getTextBounds()),
//				Adafruit_BusIO () (added explicit casts (const to non-const) needed to prevent pic32 gcc from barfing errors)
// Libraries: Our Modified libs to fix very slow pic32 SPI speed:
//				Adafruit_ILI9341 (Newer version 1.5.9 fixes SPI speed on pic32 build.)
//
// ------------------------------------------------------------------
// v1.00 Simple ISR to measure IR pulse times and record in an array.
// v1.01 After measuring; pattern is:  sync, sync, Horz, Vert, sync, sync,....
//		Now can display Th or Tv times on LCD.
// v1.02 Add second sensor for measuring distance from lighthouse. But: ISR state Never gets to Idle state.
//		Verified with oscilloscope that both sensors are detecting IR.
// v1.03 No Signal - add debug info about ISR states and counts - S1 always 61-81 uSec for Horiz time.
//		(oops - forgot tantalum cap across sensor to prevent ground bounce issue due to long cable).
// v1.04 Distance measurement works. Added separate pulse length variables in the ISR routines,
//		added more data dumping out serial port.
// v1.05 Change measurement mode to wait for sync frames before capturing timing.
//		(s command toggles it.) Add help menu to serial port.
// v1.06 General clean up. Change a few variables to const since we are using
//		a CPU where const actual keeps data in flash (PIC32 instead of AVR).
//		  Split out MC145000 LCD driver code into separate cpp and h files. (prepare to move to TFT LCD).
// v1.07 Switch to ILI9341 2.4" TFT display.  Had to fix a bug in Adafruit_GFX_Library lib
//		in getTextBounds() always returning width of 65535.
// v1.08 Faster LCD update rate: Modify Adafruit_ILI9341 lib (Adafruit_ILI9341.h/.cpp)
//		to set SPI rate higher for PIC32 devices.
//		(SPI lib was defaulting to using DIV5, now my modified Adafruit lib sets DIV2).
//		e.g. #if defined (ARDUINO_ARCH_PIC32) SPI.beginTransaction(SPISettings(SPI_CLOCK_DIV2, MSBFIRST, SPI_MODE0));
// v1.09 Add units to display values, add degrees of beam sweep display.
// v1.10 Add degree info out serial port. Added limit check in degree calculation.
// v1.11 Update comments and Fix build problems with Arduino IDE 1.8.x. and latest pic32 BSP.
//		Had to fix C++ coding errors in the Adafruit_BusIO, Adafruit_GFX_Library libraries.
// v1.12 Minor efficiency improvement in state transitions in sensor ISR handlers.
//		More tab to space formatting clean up.
// v1.13 Calculate and display x,y,z relative to center line of the Vive lighthouse
//		Improve sensor ISR latency by making it higher priority than CoreTimer.
//		Add Support for board ARDUINO_NUCLEO_G474RE - pinmap tested. TFT untested.
//		Add nano second resolution option for IR pulse measuring, this allows sub 1 mm resolving.
// v1.14 On ARDUINO_NUCLEO_G474RE swap TFT RST and CD pin mapping.
//		On ARDUINO_NUCLEO_G474RE Fix SIMSRC by moving it to pin PC9.
//		On ARDUINO_NUCLEO_G474RE Fixed wrong ISR handler mapping.
//		Enhanced IR simulator output pin control.
//		Add fritzing drawings and project files for both target boards.
// v1.15 Add more advanced IR Signal simulator generator feature for testing without a Vive lighthouse;
//		Independent S1 and S2 sweep beam simulation times and output pins.
//		BTW: Using Newer version of Adafruit_ILI9341 (1.5.9) that has higher SPI rate on Fubarino_SD (pic32)
//		board such that don't need our SPI fix anymore.
//		Using Newer version of Adafruit_GFX_Library (1.10.12) that fixed bug in getTextBounds(),
//		but still needs min()/max() macro's defined for pic32 builds. Bug in pic32 gcc include files??
//		Using Adafruit BusIO v1.9.3 with our added explicit casts to fix pic32 builds.
// v1.16 Add support for board ARDUINO_NUCLEO_L476RG.
// v1.17 Changed all calibration time values to nanosecond units to improve accuracy (sub-1mm possible with G474RE board).
//		Renamed time related variables to remove uSec reference.
// v1.18 Add CHANGELOG file to allow CI to complete release deploy phase.
// v1.19 Renamed more local variables to remove uS units.
//		Update comments.
// v1.20 Remove use of 'register' keyword since deprecated since C++17.
//		Update comments about STM32 BSP versions vs Arduino IDE versions.
//
#define VERSION "v1.20 "
#define BUILDTIME ( __DATE__ " " __TIME__ )

// -----------------------------------------------------
// Mark's debug macro
// -----------------------------------------------------
//		put any debug only code inside the parenthesis (represented by 'a') in below macro.
//		'a' can be multiple statements on multiple lines.
//		comment out next line to turn off all debug code.
#define G_DEBUG(a)  a;
#ifndef G_DEBUG
// 		if G_DEBUG() is commented out above, then define NULL debug macro that discards code 'a'.
#define G_DEBUG(a) ;;;
#endif
//		use this macro to temporarily disable an individual G_DEBUG() macro group of code by renaming it to this:
#define G_DEBUG_N(a) ;;;
// define special one for ISR debug
#define G_DEBUG_ISR(a)  a;
#ifndef G_DEBUG_ISR
//		if G_DEBUG_ISR() is commented out above, then define NULL debug macro that discards code 'a'.
#define G_DEBUG_ISR(a) ;;;
#endif
// define special one for TFT debug
#define G_DEBUG_TFT(a)	G_DEBUG(a)
#ifndef G_DEBUG_TFT
//		if G_DEBUG_TFT() is commented out above, then define NULL debug macro that discards code 'a'.
#define G_DEBUG_TFT(a) ;;;
#endif

// -----------------------------------------------------
// END -- Mark's debug macro
// -----------------------------------------------------

// -----------------------------------------------------

#include <Arduino.h>
#include "IR_Vive.h"
#include "simgen.h"			// our IR signal simulation generator.

#if defined(ARDUINO_NUCLEO_G474RE)
#include "g474re_clock.h"	// For Nucleo board
#define STM32_BOARD
#endif
#if defined(ARDUINO_NUCLEO_L476RG)
#include "l476rg_clock.h"	// For Nucleo board
#define STM32_BOARD
#endif

// TFT LCD Driver stuff
#include <SPI.h>
#include <Adafruit_GFX.h>		// our Modified version to fix pic32 gcc compile errors
#include <gfxfont.h>
#include <Adafruit_ILI9341.h>	// our modified version with (ARDUINO_ARCH_PIC32) to increase SPI clock

#define NEWLCD 1				// using a Hiletgo 2.8" TFT display
// ------------------------------------------
//		Pinmap Definitions
// ------------------------------------------

#if defined(_BOARD_FUBARINO_SD_)
// ------------------------------------------
//		FUBARINO Definitions
// ------------------------------------------
// debug pin for oscilloscope syncing
#define OS_SYNC		30	// Pin A14 on Fubarino SD silk screen
#define IRtestPin	19	// - for measuring ISR timing on a oscilloscope. Toggled in the ISR.

// Fubarino SPI info: https://github.com/fubarino/fubarino.github.com/wiki/SD-Board-pin-features

// For Fubarinuo use HW SPI pins
#define TFT_RST		11
#define TFT_DC		12	// data/cmd mode
#define TFT_CS		27

// for bitbang SPI
#define TFT_MOSI	26
#define TFT_MISO	25
#define TFT_SCK		24

// Sensor S1 and S2 interrupt inputs pins
// On the FubarinoSD board: There are 5 external interrupts on PIC32:
// INT0 = Pin4 (Vec 3), INT1 = Pin0 (Vec 7), INT2 = Pin1 (Vec 11), INT3 = Pin2 (Vec 15), INT4 = Pin3 (Vec 19)
// Use Fubarino predefined pin names:
// Sensor 1 is on PIN_INT1  Pin 0
// Sensor 2 is on PIN_INT2  Pin 1

// define board specific clock data
#define f_NANOS_PER_SYSTICK	(25.0)	// 40 MHz timer clock
#define i_NANOS_PER_SYSTICK (25ul)
// END --- #if defined(_BOARD_FUBARINO_SD_)

#elif defined(ARDUINO_NUCLEO_G474RE)
// ------------------------------------------
//		NUCLEO_G474RE Definitions
// ------------------------------------------
#if !defined(PIN_LED1)
#define PIN_LED1	PB2	/* D29  PB2 on CN10 pin 22 next to GND pin 20. */
#endif

// debug pin for oscilloscope observation
#define OS_SYNC		PC8		// PIN_A10 (D42) PC8 on CN10 pin 2 - pin for oscilloscope syncing. Toggle each loop() cycle
#define IRtestPin	PC10	// D16 PC10 on CN7 pin 1 - for measuring ISR timing on a oscilloscope. Toggled in the ISR.
// TFT pins are on Arduino connectors of the Nucleo board such that TFT shield can be used.
// Other pins on Morpho connectors CN7 & CN10:  for oscilloscope outputs, LEDs, Sensor inputs, config inputs
// For TFT use HW SPI pins
#define TFT_RST		9		// D9 PC7
#define TFT_DC		8		// D8 PA9  data/cmd mode
#define TFT_CS		10		// D10 PB6

// for hw SPI - do not need to pass these pins in to constructor. For documentation purpose only.
#define TFT_MOSI	PIN_SPI_MOSI	// D11 PA7
#define TFT_MISO	PIN_SPI_MISO	// D12 PA6  SPI1
#define TFT_SCK		PIN_SPI_SCK		// D13 PA5  Has LD2 connected, but Hi-Z driver so OK. 21.4 MHz clock rate.

// Sensor S1 and S2 interrupt inputs pins: PIN_INT1 & PIN_INT2
// PFO,PF1 not available due to 24 MHz xtal on these pins.
#define PIN_INT1	PC2		// D27 PC2  on CN7 pin 35
#define PIN_INT2	PC3		// D28 PC3  on CN7 pin 37

// special output pin for testing system clock frequency
#define MCO_OUT		PA8		// D7

// define board specific clock data
#define f_NANOS_PER_SYSTICK	((float)STM32_CLOCK_NANOS)
#define i_NANOS_PER_SYSTICK ((uint32_t)STM32_CLOCK_NANOS)
// END --- #if defined(ARDUINO_NUCLEO_G474RE)

#elif defined(ARDUINO_NUCLEO_L476RG)
// ------------------------------------------
//		NUCLEO_L476RG Definitions
// ------------------------------------------
#if !defined(PIN_LED1)
#define PIN_LED1	PB2		// D29  PB2 on CN10 pin 22 next to GND pin 20.
#endif

// debug pin for oscilloscope observation
#define OS_SYNC		PC8		// PIN_A10 (D42) PC8 on CN10 pin 2 - pin for oscilloscope syncing. Toggle each loop() cycle
#define IRtestPin	PC10	// D16 PC10 on CN7 pin 1 - for measuring ISR timing on a oscilloscope. Toggled in the ISR.
// TFT pins are on Arduino connectors of the Nucleo board such that TFT shield can be used.
// Other pins on Morpho connectors CN7 & CN10:  for oscilloscope outputs, LEDs, Sensor inputs, config inputs
// For TFT use HW SPI pins
#define TFT_RST		9		// D9 PC7
#define TFT_DC		8		// D8 PA9  data/cmd mode
#define TFT_CS		10		// D10 PB6

// for hw SPI - do not need to pass in to constructor. For documentation purpose only.
#define TFT_MOSI	PIN_SPI_MOSI	// D11 PA7
#define TFT_MISO	PIN_SPI_MISO	// D12 PA6  SPI1
#define TFT_SCK		PIN_SPI_SCK		// D13 PA5  Has LD2 connected, but Hi-Z driver so OK. 21.4 MHz clock rate.

// Sensor S1 and S2 interrupt inputs pins: PIN_INT1 & PIN_INT2
// PH0 & PH1 can't be used due to external xtal on these pins.
// Todo: PC0 & PC1 can be used on morpho pins.
//#define PIN_INT1	PC0		// D27 PC0  on CN7 pin 38
//#define PIN_INT2	PC1		// D28 PC1  on CN7 pin 36
#define PIN_INT1	PC2		// D27 PC2  on CN7 pin 35
#define PIN_INT2	PC3		// D28 PC3  on CN7 pin 37

// special output pin for testing system clock frequency
#define MCO_OUT		PA8		// D7

// define board specific clock data
#define f_NANOS_PER_SYSTICK	((float)STM32_CLOCK_NANOS)
#define i_NANOS_PER_SYSTICK ((uint32_t)STM32_CLOCK_NANOS)
// END --- #if defined(ARDUINO_NUCLEO_L476RG)
#else
#error "IR_Vive: Unsupported board - you either have the wrong board selected, or you need to create your own customization for your board."
#endif


// Use hardware SPI (on Fubarino and nucleo)
Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC,TFT_RST);

// PIC32: my modified version to set SPI clock speed. (not needed anymore).
//Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC,TFT_RST,24000000);
// If using the breakout, change pins as desired
// bitbang method:
//Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC, TFT_MOSI, TFT_SCK, TFT_RST, TFT_MISO);


// -----------------------------------------------------
// -----------------------------------------------------
// -----------------------------------------------------

// global vars for ISR for INT1
volatile  uint32_t ir1_lastRiseTime_ticks = 0;	// set by ISR - time stamp of the last rising edge
volatile  uint32_t ir1_StartTime_ticks = 0;		// set by irStart() - not used currently.
volatile  uint8_t ir1_field = 0; 				// contains which field is being received.
												// Set to initial value by ISR when stateMachine enters WaitHeaderBit1.
volatile  uint32_t ir1_count = 0;				// count of interrupt events.
uint32_t  ir1_FieldTimes[18]=
	{1,1997000*UNITS_SCALE,2778000*UNITS_SCALE,3999};	// length of time for each field. [0] has sync time, [1] will be horizontal time, [2] vertical time.
uint32_t ir1_syncCount = 0;						// count of sync pulses.

// global vars for ISR for INT2
volatile  uint32_t ir2_lastRiseTime_ticks = 0;	// set by ISR  time stamp of the last rising edge
volatile  uint32_t ir2_StartTime_ticks = 0;		// set by irStart() - not used currently.
volatile  uint8_t ir2_field = 0;				// contains which field is being received.
												// Set to initial value by ISR when stateMachine enters WaitHeaderBit1.
volatile  uint32_t ir2_count = 0;
uint32_t  ir2_FieldTimes[18]=
	{2,2169000*UNITS_SCALE,2778000*UNITS_SCALE,3999};	// length of time for each field. [0] has sync time, [1] will be horizontal time, [2] vertical time.
uint32_t ir2_syncCount = 0;						// count of sync pulses.

// Index positions for arrays ir1_FieldTimes[] and ir2_FieldTimes[]
//  [0] - has sync frame duration time
//  [1] - is horizontal time
//  [2] - is vertical time
#define T_SYNC	0
#define T_HORIZONTAL 1
#define T_VERTICAL 2

bool bWaitSyncMeasure = true;			// if true, ISR will wait for sync before measuring.

volatile  uint32_t ir1_PulseLen_ticks=0;// Used by ISR to determine length of pulse sequence for one field. (S1)
volatile  uint32_t ir2_PulseLen_ticks=0;// Used by ISR to determine length of pulse sequence for one field. (S2)
volatile  uint32_t ir1TimeNow_ticks=0;
volatile  uint32_t ir2TimeNow_ticks=0;

// v1.02 dx measurement additions
#define nanoPerMM	(9659ul)			// nSec per mm of beam sweep speed @ calibration distance from LightHouse (LH)
#define s1s2_2hMM	(66ul)				// mm spacing between S1 & S2 (was 132mm on proto 1)
#define s1s2Td	((nanoPerMM*s1s2_2hMM)) // nSec pulse time delta @ calibration distance from LH
float f_calib_2h=6.6;					// cm spacing between S1 & S2 IR sensors
int32_t calib_S1S2Time=s1s2Td*UNITS_SCALE;// pulse delta between sensors S1 & S2 spaced 2h apart @ calibration distance from LH
int32_t calib_Dx = 270;					// mm Calibration Distance: S1 / S2 plane to laser spinner in lighthouse.
int32_t S1S2_deltaTime=217000*UNITS_SCALE;	// S2 - S1 time. Initialize to "fake" demo value.
int32_t Horiz_deltaTime=0;				// +/- time from lighthouse center line
int32_t Vert_deltaTime=0;				// +/- time from lighthouse center line
int32_t dx = 1004;						// dx is the radius distance in mm from lighthouse front plane. Initialize to "fake" value for demo
float f_lh_x = 0.0;						// the x axis coordinate of the sensor platform relative to the lighthouse center line.
float f_lh_y = 0.0;						// the y axis coordinate of the sensor platform relative to the lighthouse center line.
float f_lh_z = 0.0;						// the z axis coordinate of the sensor platform relative to the lighthouse center line.

int32_t arc_time_horiz = 0;				// time in uSec between sync pulse and horizontal pulse.
int32_t arc_time_vert = 0;				// time in uSec between sync pulse and vertical pulse.

// fan beam sweep angle measurements
float f_s1s2_degrees = 7.5;				// degrees of sweep between sensors S1 and S2. Initialize to "fake" value for demo

										// degree per nSec or uSec @ calibration distance.
const float f_calib_DegreePerTimeUnit=(0.0000216*(float)(1/UNITS_SCALE));
float f_horiz_degrees = 0.0;			// degrees left or right of lighthouse center line
float f_vert_degrees = 0.0;				// degrees below or above the lighthouse center line

// Define state machine states. Do not change order of states.
enum ir_states
{
	IR_WaitSyncMeasure,
	IR_measure,					// measure pulse gaps only.
	IR_WaitSyncField,
	IR_GetHorizontalField,
	IR_WaitVerticalField,
	IR_GetVerticalField,
	IR_idle,
	IR_UnInitialized,
};

// Define ++ operators to allow us to increment the state variable.
// ir_states prefix++
inline ir_states& operator++(ir_states& irstate) {
	return irstate = static_cast<ir_states>( static_cast<int>(irstate)+1 );
}

// ir_states postfix++
inline ir_states operator++(ir_states& irstate, int) {
	ir_states tmp(irstate);
	++irstate;
	return tmp;
}


volatile uint32_t ir2_state = IR_UnInitialized;
volatile uint32_t ir1_state = IR_UnInitialized;

// Return a string identifying the states of our ISR state machine
const char* irStateString(ir_states st)
{
	switch(st){
	case IR_WaitSyncMeasure:
		return "WtMsr";
	case IR_measure:			// measure pulse gaps only.
		return "Msr";
	case IR_WaitSyncField:
		return "WtSync";
	case IR_GetHorizontalField:
		return "Hrz";
	case IR_WaitVerticalField:
		return "WtVrt";
	case IR_GetVerticalField:
		return "Vrt";
	case IR_idle:
		return "Idle";
	case IR_UnInitialized:
		return "UnInit";
	}
	return "Unknown";
}
// ---------------------------------------------------
// ir1_ISR
//
//  IR hardware interrupt service routine
//  this is called each time the IR input pin PIN_INT1 changes
//  from logic state (LOW to HIGH).
//  The Falling edge is what we care about.
//  It implements the IR pulse time grabbing state machine.
//  each pulse time field is stuffed into the array
//  ir1_FieldTimes[]
//
//  In normal mode:
//  [0] - has sync frame duration time
//  [1] - is horizontal time
//  [2] - is vertical time
//
//  In RAW measure mode (bWaitSyncMeasure true):
//  [0] - sync empty frame 1 time
//  [1] - sync empty frame 2 time
//  [2] - Horizontal time (sync to horizontal pulse)
//  [3] - Time from Horizontal pulse to next sync pulse
//  [4] - Vertical time (sync to vertical pulse)
//  [5] - Time from Vertical pulse to next sync pulse
//  [6] - repeat of above....
//
// On the ARDUINO_NUCLEO_G474RE platform this ISR executes in 1 uSec,
// there is a latency of 1.8 uSec from the rising edge on the pin until this ISR starts.
//
void ir1_ISR( )
{
	ir1TimeNow_ticks = fastTicks();
	ir1_PulseLen_ticks = ir1TimeNow_ticks - ir1_lastRiseTime_ticks;

	G_DEBUG_ISR( digitalWrite(IRtestPin, HIGH); );

	switch(ir1_state)
	{
		case IR_WaitSyncMeasure:
			if( ir1_PulseLen_ticks < (PR_SYNC_UPPER) && ir1_PulseLen_ticks > (PR_SYNC_LOWER) )
			{
				// have valid sync time,
				ir1_FieldTimes[ir1_field++] = ir1_PulseLen_ticks;
				ir1_syncCount++;
				if(ir1_syncCount>1)
					ir1_state = IR_measure;	// if have 2 sync go measure rest.
			}else
			{
				// not a valid sync, keep waiting here
				ir1_state = IR_WaitSyncMeasure;
				ir1_syncCount=0;
				ir1_field=0;
			}
		break;

		case IR_measure:
			ir1_FieldTimes[ir1_field++] = ir1_PulseLen_ticks;
			if(ir1_field > 15)
				ir1_state = IR_idle;
		break;

		case IR_WaitSyncField:
			ir1_field = 0;
			// check if sync 1 timing constraint was met.
			if( ir1_PulseLen_ticks < (PR_SYNC_UPPER) && ir1_PulseLen_ticks > (PR_SYNC_LOWER) )
			{
				// have valid sync time, check if go measure Horizontal time.
				++ir1_state;	// IR_GetHorizontalField;
				ir1_syncCount=1;
			}else
			{
				// not a valid sync, keep waiting here
				ir1_syncCount=0;
			}
			ir1_FieldTimes[ir1_field++] = ir1_PulseLen_ticks;
		break;

		case IR_GetHorizontalField:
			// check if  timing constraint was met.
			if( ir1_PulseLen_ticks < (PR_SYNC_LOWER-PRS_TOLERANCE) )
			{
				// have valid horizontal time, wait for this time field to end.
				++ir1_state;	// IR_WaitVerticalField;
				ir1_FieldTimes[ir1_field++] = ir1_PulseLen_ticks; // save horizontal time.
			}else
			{
				// not a valid time, wait for next sequence.
				ir1_syncCount++;
			}
		break;

		case IR_WaitVerticalField:
			if( ir1_PulseLen_ticks < (PR_SYNC_LOWER-PRS_TOLERANCE) )
			{
				// have valid time, wait for vertical time pulse.
				++ir1_state;	// IR_GetVerticalField;
			}else
			{
				// not a valid time, wait for next sequence.
				ir1_state = IR_WaitSyncField;
			}
		break;

		case IR_GetVerticalField:
			// check if  timing constraint was met.
			if( ir1_PulseLen_ticks < (PR_SYNC_LOWER-PRS_TOLERANCE) )
			{
				// have valid vertical time, go idle..
				++ir1_state;	// IR_idle;
			}else
			{
				// not a valid time, wait for next sequence.
				ir1_state = IR_WaitSyncField;
			}
			ir1_FieldTimes[ir1_field++] = ir1_PulseLen_ticks;	// save vertical time.
		break;

		case IR_idle:
			ir1TimeNow_ticks = ir1_lastRiseTime_ticks;			// don't mess with ir1_lastRiseTime_ticks on exit.
		break;

		default:	// state machine is in an invalid state.
			ir1_state = IR_UnInitialized;
		break;

	}
	ir1_lastRiseTime_ticks = ir1TimeNow_ticks;

	G_DEBUG_ISR( digitalWrite(IRtestPin, LOW); );
}

// return ir1 state machine state
ir_states get_ir1_state(void)
{
	return (ir_states)ir1_state;
}
// return which sync field is active
uint32_t get_ir1_syncCount()
{
	return ir1_syncCount;
}
// --------------------------------------
// protected access to ir1_StartTime_ticks
uint32_t get_ir1_StartTime_ticks()
{
	uint32_t val;
	noInterrupts();   			// disable interrupts
	val = ir1_StartTime_ticks;	// access the shared data
	interrupts();
	return val;
}
// protected access to ir1_lastRiseTime_ticks
uint32_t get_ir1_lastRiseTime_ticks()
{
	uint32_t val;
	noInterrupts();   			// disable interrupts
	val = ir1_lastRiseTime_ticks;// access the shared data
	interrupts();
	return val;
}


// ---------------------------------------------------
// ir2_ISR
//
//  IR hardware interrupt service routine
//  this is called each time the IR input pin PIN_INT2 changes
//  from logic state ( LOW to HIGH).
//  The Falling edge is what we care about.
//  It implements the IR pulse time grabbing state machine.
//  each pulse time field is stuffed into the array
//  ir2_FieldTimes[]
//
//  In normal mode:
//  [0] - has sync frame duration time
//  [1] - is horizontal time
//  [2] - is vertical time
//
//  In RAW measure mode (bWaitSyncMeasure true:
//  [0] - sync empty frame 1 time
//  [1] - sync empty frame 2 time
//  [2] - Horizontal time (sync to horizontal pulse)
//  [3] - Time from Horizontal pulse to next sync pulse
//  [4] - Vertical time (sync to vertical pulse)
//  [5] - Time from Vertical pulse to next sync pulse
//  [6] - repeat of above....
//
// On the ARDUINO_NUCLEO_G474RE platform this ISR executes in 1 uSec,
// there is a latency of 1.8 uSec from the rising edge on the pin until this ISR starts.
//
void ir2_ISR( )
{
	ir2TimeNow_ticks = fastTicks();
	ir2_PulseLen_ticks = ir2TimeNow_ticks - ir2_lastRiseTime_ticks;

	G_DEBUG_ISR( digitalWrite(IRtestPin, HIGH); );

	switch(ir2_state)
	{
		case IR_WaitSyncMeasure:
			if( ir2_PulseLen_ticks < (PR_SYNC_UPPER) && ir2_PulseLen_ticks > (PR_SYNC_LOWER) )
			{
				// have valid sync time,
				ir2_FieldTimes[ir2_field++] = ir2_PulseLen_ticks;
				ir2_syncCount++;
				if(ir2_syncCount>1)
					ir2_state = IR_measure;	// if have 2 sync go measure rest.
			}else
			{
				// not a valid sync, keep waiting here
				ir2_syncCount=0;
				ir2_field=0;
			}
		break;

		case IR_measure:
			ir2_FieldTimes[ir2_field++] = ir2_PulseLen_ticks;
			if(ir2_field > 15)
				ir2_state = IR_idle;
		break;

		case IR_WaitSyncField:
			ir2_field = 0;
			// check if sync 1 timing constraint was met.
			if( ir2_PulseLen_ticks < (PR_SYNC_UPPER) && ir2_PulseLen_ticks > (PR_SYNC_LOWER) )
			{
				// have valid sync time, check if go measure Horizontal time.
				ir2_state++;	// IR_GetHorizontalField;
			}else
			{
				// not a valid header, keep waiting here (v0.08 changed from WaitHeaderStart)
			}
			ir2_FieldTimes[ir2_field++] = ir2_PulseLen_ticks;
		break;

		case IR_GetHorizontalField:
			// check if  timing constraint was met.
			if( ir2_PulseLen_ticks < (PR_SYNC_LOWER-PRS_TOLERANCE) )
			{
				// have valid horizontal time, wait for this time field to end.
				ir2_state++;	// IR_WaitVerticalField;
				ir2_FieldTimes[ir2_field++] = ir2_PulseLen_ticks; // save horizontal time.
			}else
			{
				// not a valid time, wait for next sequence.
				ir2_state = IR_GetHorizontalField; //IR_WaitSyncField;
			}
		break;

		case IR_WaitVerticalField:
			if( ir2_PulseLen_ticks < (PR_SYNC_LOWER-PRS_TOLERANCE) )
			{
				// have valid time, wait for vertical time pulse.
				ir2_state++;	// IR_GetVerticalField;
			}else
			{
				// not a valid time, wait for next sequence.
				ir2_state = IR_WaitSyncField;
			}
		break;

		case IR_GetVerticalField:
			// check if  timing constraint was met.
			if( ir2_PulseLen_ticks < (PR_SYNC_LOWER-PRS_TOLERANCE) )
			{
				// have valid vertical time, go idle..
				ir2_state++;	// IR_idle;
			}else
			{
				// not a valid time, wait for next sequence.
				ir2_state = IR_WaitSyncField;
			}
			ir2_FieldTimes[ir2_field++] = ir2_PulseLen_ticks;	// save vertical time.
		break;

		case IR_idle:
			ir2TimeNow_ticks = ir2_lastRiseTime_ticks;			// don't mess with ir2_lastRiseTime_ticks on exit.
		break;

		default:	// state machine is in an invalid state.
			ir2_state = IR_UnInitialized;
		break;

	}
	ir2_lastRiseTime_ticks = ir2TimeNow_ticks;

	G_DEBUG_ISR( digitalWrite(IRtestPin, LOW); );
}
// return ir2 state machine state
ir_states get_ir2_state(void)
{
    return (ir_states)ir2_state;
}

// --------------------------------------
// protected access to ir2_StartTime_ticks
uint32_t get_ir2_StartTime_ticks()
{
	uint32_t val;
	noInterrupts();   				// disable interrupts
	val = ir2_StartTime_ticks;		// access the shared data
	interrupts();
	return val;
}
// protected access to ir2_lastRiseTime_ticks
uint32_t get_ir2_lastRiseTime_ticks()
{
	uint32_t val;
	noInterrupts();   				// disable interrupts
	val = ir2_lastRiseTime_ticks;  	// access the shared data
	interrupts();
	return val;
}


/*
 * return high resolution tick count.
 * Called from ISR with higher priority than system clock.
 * Therefore no need to disable interrupts.
 *
 * On Fubarino each tick is 25.0 nanoseconds. Each tick is 1/2 CPU Core clock rate.
 * Number of CoreTimer ticks per microsecond, for micros() function
   #define CORETIMER_TICKS_PER_MICROSECOND		(F_CPU / 2 / 1000000UL)
	40 per microsecond, which is 25 nSec
	* See wiring.c in pic32 package.

 * On Nucleo G474RE each tick is 7.979 nanoseconds. 125.124 MHz sysClock
 * On Nucleo L476RG each tick is 12.99 nanoseconds.  77.000 Mhz sysClock
*/
inline uint32_t fastTicks(void)
{
#if defined(NANOTICKS)
#if defined(_BOARD_FUBARINO_SD_)
	uint32_t cur_timer_val	=	0;
	uint32_t result;
	// globals in pic32 BSP wiring.c
	extern volatile unsigned long gTimer0_millis ;
	extern volatile unsigned long gCore_timer_last_val;
	result = gTimer0_millis * 1000ul * 1000ul; 			// convert to nano seconds
	cur_timer_val = readCoreTimer();
	cur_timer_val -= gCore_timer_last_val;
	cur_timer_val = cur_timer_val * i_nanoPerFastTick();// convert to nSec
	return (result + cur_timer_val);
#endif
#if defined(STM32_BOARD)
	return nanos();				// STM32 sysTick clock to get nSec resolution
#endif
#else
	return micros();			// using micro second resolution.
#endif
}

/*
 * Get floating point number of nano seconds per system timer clock cycle.
 */
inline float f_nanoPerFastTick(void)
{
#if defined(NANOTICKS)
#if defined(_BOARD_FUBARINO_SD_)
	return f_NANOS_PER_SYSTICK;	// 40 MHz coreTimer clock
#endif
#if defined(STM32_BOARD)
	return f_NANOS_PER_SYSTICK;	// stm32 sysTick clock
#endif
#else
								// no board specific high res timer
	return 1000.0;				// using micro second resolution.
#endif
}

/*
 * Get integer number of nano seconds per system timer clock cycle.
 */
inline uint32_t i_nanoPerFastTick(void)
{
#if defined(NANOTICKS)
#if defined(_BOARD_FUBARINO_SD_)
	return i_NANOS_PER_SYSTICK;	// 40 MHz coreTimer clock
#elif defined(STM32_BOARD)
	return i_NANOS_PER_SYSTICK;	// 125.142 MHz or 77.0 MHz sysTick clock
#else
#error	"unsupported board"
#endif
#else
	return 1000ul;				// using micro second resolution.
#endif
}

#if defined(_BOARD_FUBARINO_SD_)
// an array of ISR subroutines. Index corresponds to interrupt number on Fubaruino board (1 & 2).
typedef void (*isr_ptr_t)( void );
isr_ptr_t  A_irISRfn[]={ir1_ISR, ir1_ISR, ir2_ISR, ir1_ISR, ir1_ISR};
#endif
#if defined(ARDUINO_NUCLEO_G474RE)
// Array order for int pins assigned in Nucleo board. We use index 2 & 3 for PC2, PC3 respectively
typedef void (*isr_ptr_t)( void );
isr_ptr_t  A_irISRfn[]={ir1_ISR, ir2_ISR, ir1_ISR, ir2_ISR, ir1_ISR};
const IRQn_Type irq_n[] = {EXTI0_IRQn,EXTI1_IRQn, EXTI2_IRQn, EXTI3_IRQn, EXTI4_IRQn};
#endif
#if defined(ARDUINO_NUCLEO_L476RG)
// Array order for int pins assigned in Nucleo board. We use index 2 & 3 for PC2, PC3 respectively
typedef void (*isr_ptr_t)( void );
isr_ptr_t  A_irISRfn[]={ir1_ISR, ir2_ISR, ir1_ISR, ir2_ISR, ir1_ISR};
const IRQn_Type irq_n[] = {EXTI0_IRQn,EXTI1_IRQn, EXTI2_IRQn, EXTI3_IRQn, EXTI4_IRQn};
#endif
// ---------------------------------------------------
// ---------------------------------------------------
// ir_setup()
// Setup the ISR Handler and priority for the S1 & S2 IR sensors
//
// On the FubarinoSD board: There are 5 external interrupts on PIC32:
// INT0 = Pin4, PIN_INT0	(4)
// INT1 = Pin0, PIN_INT1	(0)
// INT2 = Pin1, PIN_INT2	(1)
// INT3 = Pin2, PIN_INT3	(2)
// INT4 = Pin3, PIN_INT4	(3)
//
//  Input: IRpin - values: PIN_INT0, PIN_INT1, PIN_INT2, PIN_INT3, PIN_INT4
//
// On the Nucleo G474RE board there are 16 external interrupts that
// can be mapped to any port pin with the same bit number. e.g Int 0 to PC0, Int 1 to PA1, etc.
// Due to the STM32 Arduino BSP, we will only use 0 thru 3 which map in order
// with the numeric values for pins PF0, PF1, PC2, PC3.
// See pin definitions in BSP file variant_NUCLEO_G47RE.h
// Note: PF0 & PF1 are not usable since they are connected to an external crystal.
//
//  Call this from within the Arduino setup() function.
// ---------------------------------------------------
// ---------------------------------------------------
void ir_setup(int IRpin)
{
	// initialize state machine
	ir_stopDx();

	#if defined(_BOARD_FUBARINO_SD_)
	int INT_numb = (IRpin+1)&0x03;
	// initialize hardware interface
	// Make IRpin an input and set pull up resistor:
	pinMode( IRpin, INPUT_PULLUP );
	// hookup ISR
	attachInterrupt(INT_numb, A_irISRfn[INT_numb], RISING); // Register interrupt function on IntX

	// To lower jitter in our sensor time measurements, adjust ISR priority to be higher than CoreTimer
	int isr_vec_numb = 3 + 5*(INT_numb);
	setIntPriority( isr_vec_numb , _CT_IPL_IPC, _CT_SPL_IPC+1);
	#endif

	#if defined(ARDUINO_NUCLEO_G474RE)
	int INT_numb = IRpin - PF0;	// INT_numb must be 0 - 3. With corresponding pins: PF0, PF1, PC2, PC3
	// initialize hardware interface
	// Make IRpin an input and set pull up resistor:
	pinMode( IRpin, INPUT_PULLUP );
	// hookup ISR
	attachInterrupt(IRpin, A_irISRfn[INT_numb], RISING); // Register interrupt function on IntX
	// ISR number can be for pin 0 : EXTI0_IRQn, for pin 1 EXTI1_IRQn, for pin 2 : EXTI2_IRQn, .. to pin 3.
	//  See interrupts.cpp in .../2.1.0/libraries/SrcWrappers/src/stm32/
	// Default Priority lowest in stm32 BSP, make it higher than systick which is lowest..
	// To lower jitter in our sensor time measurements, adjust ISR priority to be higher than CoreTimer
	HAL_NVIC_SetPriority(irq_n[INT_numb], 1, EXTI_IRQ_SUBPRIO);
	#endif

	#if defined(ARDUINO_NUCLEO_L476RG)
	int INT_numb;	// INT_numb must be 0 - 3
	if( IRpin == PC0 )
		INT_numb = 0;
	else if( IRpin == PC1 )
		INT_numb = 1;
	else
		// With corresponding pins:
		//  L476: PH0, PH1, PC2, PC3   see pin numbers in stm32 BSP file: variant_NUCLEO_L476RG.h
		INT_numb = IRpin - PH0;
	// initialize hardware interface
	// Make IRpin an input and set pull up resistor:
	pinMode( IRpin, INPUT_PULLUP );
	// hookup ISR
	attachInterrupt(IRpin, A_irISRfn[INT_numb], RISING); // Register interrupt function on IntX
	// ISR number can be for pin 0 : EXTI0_IRQn, for pin 1 EXTI1_IRQn, for pin 2 : EXTI2_IRQn, .. to pin 3.
	//  See interrupts.cpp in .../2.1.0/libraries/SrcWrappers/src/stm32/
	// Default Priority lowest in stm32 BSP, make it higher than systick which is lowest..
	// To lower jitter in our sensor time measurements, adjust ISR priority to be higher than CoreTimer
	HAL_NVIC_SetPriority(irq_n[INT_numb], 1, EXTI_IRQ_SUBPRIO);
	#endif

G_DEBUG_ISR(
	pinMode( IRtestPin, OUTPUT);  // for toggling in the ISR to view on an oscilloscope.
	digitalWrite(IRtestPin, LOW);
)
}
void ir_stopDx()
{	// stop the IR ISR state machine.
	noInterrupts();			// disable interrupts
	ir1_PulseLen_ticks = 0;	// set to 0 so can detect first firing of ISR.
	ir2_PulseLen_ticks = 0;

	ir1_state = IR_idle;	// pause the ISR state machine
	ir1_field=0;

	ir2_state = IR_idle;	// pause the ISR state machine
	ir2_field=0;

	interrupts();
}
void ir_measure()
{	// put IR ISR state machine in raw pulse time measure mode.
	ir_stopDx();

	noInterrupts();			// disable interrupts
	ir1_StartTime_ticks = ir1_lastRiseTime_ticks = fastTicks();
	ir2_StartTime_ticks = ir1_StartTime_ticks;
	ir2_lastRiseTime_ticks = ir1_lastRiseTime_ticks;

	if(bWaitSyncMeasure){
		ir1_state = ir2_state = IR_WaitSyncMeasure;
	}else{
		ir1_state = ir2_state = IR_measure;
	}
	ir1_count = 0;
	ir2_count = 0;
	ir1_field = 0;
	ir2_field = 0;
	ir1_syncCount = 0;
	ir2_syncCount = 0;
	interrupts();
}
void ir_startDx()
{	// starts Th and Tv capture mode used for distance measuring.
	// prepare state machine to RX another IR stream.
	ir_stopDx();

	noInterrupts();			// disable interrupts
	ir1_FieldTimes[0] = 1;
	ir2_FieldTimes[0] = 1;
	ir1_StartTime_ticks = ir1_lastRiseTime_ticks = fastTicks();
	ir2_StartTime_ticks = ir2_lastRiseTime_ticks = ir1_StartTime_ticks;

	ir1_state = IR_WaitSyncField;		// start the ISR
	ir2_state = IR_WaitSyncField;		// start the ISR
	ir1_count = 0;
	ir2_count = 0;
	ir1_syncCount = 0;
	ir2_syncCount = 0;
	interrupts();
}
// =========================================================
//
//  end of IR library
//
// =========================================================


// =================================================================
// =================================================================
// cooperative multi-tasking Time delay timer
//
uint32_t timerEnd = 0;	// 0 == no timer running, if Non-zero, timer is running.

void timerClear()
{
	timerEnd = 0;		// clear the counter.
}

// ---------------------------------------
// timerMsec()
//  if no timer running, start a new elapsed timer
//  else check if previous timer has expired.
//
// input: delay
//			the desired elapsed time in mSec to start if not already running.
//				If timer currently running value is ignored.
//			0 - to only check if previous timer expired (and not start a new timer)
//
// return:  mSecs remaining before expires.  0 - if time has expired.
//
uint32_t timerMsec(uint32_t delay)
{
	if( timerEnd == 0 && delay != 0 )
	{	// no timer running, so start one.
		timerEnd = millis() + delay;
	}
	return timerMsecRemaining();
}

uint32_t timerMsecRemaining()
{
	// return the amount of time (mSec) remaining in the timer. 0 if timer expired.
	uint32_t now = millis();
	if( timerEnd == 0 || now > timerEnd )
	{
		timerClear();	// clear the counter.
		return 0;		// timer expired.
	}
	return(timerEnd - now);
}

void timerStart(uint32_t delay)
{
	// only start a new delay timer if previous one is not running.
	if( timerEnd == 0)
		timerEnd = millis() + delay;
}

uint8_t timerExpired()
{
	// check if a running timer has expired, 1 == expired, 0 == not expired.
	if(timerMsecRemaining() == 0)
	{
		return 1;		// timer expired.
	}
	return 0;
}



// =================================================================
// -----------------------------------------------------------------
//  				app code below
// -----------------------------------------------------------------
// =================================================================

// ------------------------------------------
//		Pinmap Definitions Fubarino SD
// ------------------------------------------
#if defined(_BOARD_FUBARINO_SD_)
#define SIMSRC_PIN1	20		// output used as external interrupt stimulator - for S1 ISR testing.
#define SIMSRC_PIN2	21		// output used as external interrupt stimulator - for S2 ISR testing.
#define MODE_PIN	4		// LOW to measure IR pulses, HIGH to get H and V times or distance.
#define AXIS_PIN	6		// LOW to display Horizontal, HIGH to display vertical
#define RESET_PIN	34 		// A10  LOW will reset the board (pin 34 connected to reset input)
#endif	// #if defined(_BOARD_FUBARINO_SD_)

// ------------------------------------------
//		Pinmap Definitions G474RE
// ------------------------------------------
#if defined(ARDUINO_NUCLEO_G474RE)
#define SIMSRC_PIN1	PC9		// D31 PC9  on cn10 pin 1. - An output used as external interrupt stimulator - for S1 ISR testing.
#define SIMSRC_PIN2	PC6		// D47 PC6  on cn10 pin 4. - An output used as external interrupt stimulator - for S2 ISR testing.
#define MODE_PIN	PA15	// D20 PA15 on cn7 pin 17 next to GND pin 19. - LOW to measure IR pulses, HIGH to get H and V times and distance.
#define AXIS_PIN	PB7		// D21 PB7  on cn7 pin 21 next to GND pin 22(right angle). NOT USED - HIGH to display Horizontal, LOW to display vertical
#define RESET_PIN	USER_BTN// PC13 (B1 blue BTN) - LOW will reset the board (or just use RST pin on the board)
#endif	// #if defined(ARDUINO_NUCLEO_G474RE)

// ------------------------------------------
//		Pinmap Definitions L476RG
// ------------------------------------------
#if defined(ARDUINO_NUCLEO_L476RG)
#define SIMSRC_PIN1	PC9		// D31 PC9  on cn10 pin 1. - An output used as external interrupt stimulator - for S1 ISR testing.
#define SIMSRC_PIN2	PC6		// D47 PC6  on cn10 pin 4. - An output used as external interrupt stimulator - for S2 ISR testing.
#define MODE_PIN	PA15	// D20 PA15 on cn7 pin 17 next to GND pin 19. - LOW to measure IR pulses, HIGH to get H and V times and distance.
#define AXIS_PIN	PB7		// D21 PB7  on cn7 pin 21 next to GND pin 22(right angle). NOT USED - HIGH to display Horizontal, LOW to display vertical
#define RESET_PIN	USER_BTN// PC13 (B1 blue BTN) - LOW will reset the board (or just use RST pin on the board)
#endif	// #if defined(ARDUINO_NUCLEO_L476RG)

// method to start the state machine in one of the two modes of operation
//  mode:  0 == measure IR pulse times,  1 == h/v timing / distance
void ir_go(uint8_t mode)
{
#ifdef MC145000_H
	if(mode){
		ir_startDx();
		LCDsetDigit(0,LCD_IND__DELTA);	// delta symbol on.
	}else{
		ir_measure();
		LCDsetDigit(0,LCD_IND__M);		// M symbol on.
	}
	LCDupdate();
#else
	if(mode){
		ir_startDx();
	}else{
		ir_measure();
	}
#endif
}

byte ledToggle=0;

void toggleLED(){
	ledToggle ^= 1;
	digitalWrite(PIN_LED1, ledToggle);
}

byte lastMode=0;
// get string for mode
//  mode:  0 == measure IR pulse times,  1 == h/v timing for distance and 3D location
const char* modeString(uint8_t md)
{
	if(md==0) {
		return " Raw Timing";
	}
	return " Distance";
}

#if defined (ARDUINO_ARCH_PIC32)
// These watch dog macros for chipKIT compile OK. NOT USED ANYMORE.
#define EnableWDT() (WDTCONSET = _WDTCON_ON_MASK)
#define DisableWDT() (WDTCONCLR = _WDTCON_ON_MASK)
#define ClearWDT() (WDTCONSET = _WDTCON_WDTCLR_MASK)
#define ClearEventWDT() ( RCONCLR = _RCON_WDTO_MASK)
#define ReadEventWDT() (RCONbits.WDTO)
#define ReadPostscalerWDT()  (WDTCONbits.WDTPSTA)
// define a reset function in the chipKit pic32_software_reset.S  this works, no need for watchdog method.
extern "C" void __pic32_software_reset(void);
#define resetFunc() __pic32_software_reset()

#elif defined(STM32_BOARD)
#define EnableWDT()
#define DisableWDT()
#define ClearWDT()
#define ClearEventWDT()
#define ReadEventWDT()
#define ReadPostscalerWDT()
#define resetFunc()	NVIC_SystemReset();
#else
#error	"unsupported board"
#endif

// *********************** GFX LCD variables ************************
int16_t dxX=0;				// dx number cursor location
int16_t dxY=0;
int16_t deltaX=0;			// s1s2 time delta number location
int16_t deltaY=0;
int16_t sweepX, sweepY;		// s1s2 sweep angle number location
int16_t xX,xY,yX,yY,zX,zY;	// xyz number location

#define XYZ_NUMBER_SIZE	(2)	// font size

#define NOSIG_Y 195			// no signal message location
#define NOSIG_X 40
const char noSignalString[]="No IR Signal!";

// *************************  setup   *******************************

// define << operator template for Serial use
template<class T> inline Print &operator <<(Print &obj, T arg) { obj.print(arg); return obj; }

void setup()
{
	DisableWDT();

#ifdef MC145000_H
#error Depricated: The old LCD is Not what we want.
	Serial.begin(57600);
	LCDsetup(12,11,10);				// clock, data, sync pin #
	digitalWrite(PIN_LED1, HIGH);	// Start of with LED off
	pinMode(PIN_LED1, OUTPUT);		// Make LED pin an output
	digitalWrite(SIMSRC_PIN1, LOW);	// Simulator pin must start low
	pinMode(SIMSRC_PIN1, OUTPUT);	// And simulator pin must be output too
	digitalWrite(SIMSRC_PIN2, LOW);	// Simulator pin must start low
	pinMode(SIMSRC_PIN2, OUTPUT);	// And simulator pin must be output too
	digitalWrite(RESET_PIN, HIGH);	// set our reset driver pin high before
	pinMode( RESET_PIN, OUTPUT);	// setting it to output.
	pinMode( MODE_PIN, INPUT_PULLUP );
	pinMode( AXIS_PIN, INPUT_PULLUP );
	// some LCD feed back on start up.
	LCDsetDigit(5,0xff);			// turn on all left side indicators.
	LCDsetDigit(4,0xff);			// turn on all left digit segments (# 8).
	LCDupdate();
	delay(5000);					// give 10 seconds to attach serial monitor

	LCDsetDigit(0,0xff);			// turn on all right side indicators.
	LCDupdate();
	LCDprint(5);					// show number 5
	Serial.println( "5 seconds..." );
	digitalWrite(PIN_LED1, LOW);
	delay(2000);
	digitalWrite(PIN_LED1, HIGH);
	LCDprint(3);					// show number 3
	LCDupdate();
	Serial.println("3 seconds...");
	delay(1000);
	LCDprint(2);					// show number 2
	Serial.println("2 seconds...");
	LCDsetDigit(5,0);				// turn off all left side indicators.
	LCDupdate();
	delay(1000);
	LCDprint(1);					// show number 1
	Serial.println("1 second....");
	delay(1000);
	digitalWrite(PIN_LED1, LOW);
	LCDsetDigit(0,0);				// turn off all right side indicators.
	LCDsetDigit(1,0);				// turn off all segments of right most digit
	LCDupdate();
#endif

#ifdef NEWLCD
	pinMode( IRtestPin, OUTPUT);	// for toggling in the ISR to view on an oscilloscope.
	// confirm test pin is working by toggling it
	digitalWrite(IRtestPin, HIGH);
	pinMode(OS_SYNC, OUTPUT);		// debug pin for oscilloscope triggering
	Serial.begin(230400);

	digitalWrite(OS_SYNC, HIGH);	// for oscilloscope triggering
	tft.begin();
	digitalWrite(OS_SYNC, LOW);		// for oscilloscope triggering
	tft.fillScreen(ILI9341_BLACK);
	tft.setRotation(1);
	tft.setCursor(0, 0);			// x,y
	digitalWrite(OS_SYNC, HIGH);	//  for oscilloscope triggering
	tft.setTextColor(ILI9341_WHITE);  tft.setTextSize(1);
	tft.println(VERSION);
	displayNoSignal();

	digitalWrite(PIN_LED1, HIGH);	// Start of with LED off
	pinMode(PIN_LED1, OUTPUT);		// Make LED pin an output
	digitalWrite(SIMSRC_PIN1, LOW);	// Simulator pin must start low
	pinMode(SIMSRC_PIN1, OUTPUT);	// And simulator pin must be output too
	digitalWrite(SIMSRC_PIN2, LOW);	// Simulator pin must start low
	pinMode(SIMSRC_PIN2, OUTPUT);	// And simulator pin must be output too

	digitalWrite(RESET_PIN, HIGH);	// set our reset driver pin high before
	pinMode( RESET_PIN, OUTPUT);	// setting it to output.

	pinMode( MODE_PIN, INPUT_PULLUP );
	pinMode( AXIS_PIN, INPUT_PULLUP );

		// give 5 seconds to attach serial monitor (for fubarino boot loader implementation)
	delay(5000);
		// setup the distance display area
	tft.setTextColor(ILI9341_CYAN);
	tft.setTextSize(2);
	tft.print("\nDistance: ");
	dxX = tft.getCursorX();			// save location for our dx number start
	dxY = tft.getCursorY()-24;
	tft.println(" ");				// move default cursor local down a line.
	G_DEBUG_TFT(Serial << "TFTxy " << "dx: " << dxX << ',' << dxY << "\n"; )

	Serial.println("5 seconds...");
	digitalWrite(PIN_LED1, LOW);	// LED ON
	digitalWrite(IRtestPin, LOW);	// check test pin
	delay(2000);
	displayDx(8888);				// display 8888.
	digitalWrite(IRtestPin, HIGH);	// check test pin
	digitalWrite(PIN_LED1, HIGH);	// LED OFF
	digitalWrite(SIMSRC_PIN1, HIGH);// Simulator pin test
	digitalWrite(SIMSRC_PIN2, HIGH);// Simulator pin test
	Serial.println("3 seconds...");
	delay(1000);
	Serial.println("2 seconds...");
	delay(1000);
	Serial.println("1 second....");

	delay(1000);
	digitalWrite(PIN_LED1, LOW);
	digitalWrite(IRtestPin, LOW);	// check test pin
	digitalWrite(SIMSRC_PIN1, LOW);	// Simulator pin test
	digitalWrite(SIMSRC_PIN2, LOW);	// Simulator pin test
	calculateRadius();
	displayDx(dx);					// start with a "fake" reading for demo purposes.

	tft.setTextSize(2);
	tft.println("");				// space down 1 line.

	tft.setTextColor(ILI9341_CYAN);
	tft.setTextSize(1);
	tft.print("S1S2 DeltaTime: ");
	deltaX = tft.getCursorX();		// save location for our dx number start
	deltaY = tft.getCursorY()-5;
	displayDeltaTime(S1S2_deltaTime);// start with a "fake" reading for demo purposes.
	G_DEBUG_TFT(Serial << "TFTxy " << "s1s2 delta: " << deltaX << ',' << deltaY << "\n"; )

	tft.setTextSize(1);
	tft.println("\n");				// space down 2 lines.
	tft.setTextColor(ILI9341_CYAN);
	tft.print(" Beam angle: ");
	sweepX = tft.getCursorX();		// save location for our dx number start
	sweepY = tft.getCursorY()-5;
	// calculate and set degree beam sweep (scan)
	setDegreesFromTime(S1S2_deltaTime);
	displaySweepAngle();
	G_DEBUG_TFT(Serial << "TFTxy " << "s1s2 angle: " << sweepX << ',' << sweepY << "\n"; )
	// Horz & vert degrees
	tft.setTextSize(1);
	tft.println("\n");				// space down 2 lines.
	tft.setTextColor(ILI9341_CYAN);
	tft.print("Horiz angle: ");
	tft.setTextSize(1);
	tft.println("\n");				// space down 2 lines.
	tft.setTextColor(ILI9341_CYAN);
	tft.print(" Vert angle: ");
	// X Y Z
	tft.setTextSize(XYZ_NUMBER_SIZE);
	tft.println("\n");				// space down 12 lines.
	tft.setTextColor(ILI9341_CYAN);
	tft.print("X: ");
	xX = tft.getCursorX()-2;		// save location for our dx number start
	xY = tft.getCursorY();
	G_DEBUG_TFT(Serial << "TFTxy " << "X: " << xX << ',' << xY << "\n"; )
	tft.print("0000 ");				// space for 4 digits
	tft.print(" Y: ");
	yX = tft.getCursorX()-2;		// save location for our dx number start
	yY = tft.getCursorY();
	G_DEBUG_TFT(Serial << "TFTxy " << "Y: " << yX << ',' << yY << "\n"; )
	tft.print("0000 ");				// space for 4 digits
	tft.print(" Z: ");
	zX = tft.getCursorX()-2;		// save location for our dx number start
	zY = tft.getCursorY();
	G_DEBUG_TFT(Serial << "TFTxy " << "Z: " << zX << ',' << zY << "\n"; )
	tft.print("0000");				// space for 4 digits
	digitalWrite(IRtestPin, HIGH);	// check test pin
#endif

	Serial.println( "Begin " VERSION );
	ir_setup(PIN_INT1);				// setup ISR handler and state machine.
	ir_setup(PIN_INT2);				// setup ISR handler and state machine.
	lastMode=digitalRead(MODE_PIN);	// 0 == raw measure, 1 == distance
	Serial.print("Measurement Mode: ");
	Serial.print(lastMode);
	Serial.println(modeString(lastMode));
	ir_go(lastMode);
	timerClear();
	timerMsec(NOSIG_TIMEOUT);		// ten second timer start
	init_simgen_engine(SIMSRC_PIN1,SIMSRC_PIN2);
	Serial.println("Enter ? for help menu");
}


// **********************************************************************
// erase string using current cursor location and current font size.
void eraseString(char* str){
	int16_t x1, y1;
	uint16_t w1, h1;
	int16_t locX = tft.getCursorX();	// get location of cursor
	int16_t locY = tft.getCursorY();
	tft.getTextBounds(str, locX, locY, &x1, &y1, &w1, &h1);
	tft.fillRect(x1, y1, w1, h1, ILI9341_BLACK);
}

bool noSignalOn=false;
int16_t noSigTextSize=3;

void eraseNoSignal(){
	int16_t x1, y1;
	uint16_t w1, h1;
	if(noSignalOn){
		int16_t saveX = tft.getCursorX();	// save location of cursor
		int16_t saveY = tft.getCursorY();
		w1=0;
		// clear old text
		tft.setTextSize(noSigTextSize);
		tft.setCursor(NOSIG_X, NOSIG_Y);	// x,y
		eraseString((char*)noSignalString);
		noSignalOn=false;
		// restore cursor.
		tft.setCursor(saveX, saveY);
	}
	return;
}
void displayNoSignal(){
	int16_t saveX = tft.getCursorX();		// save location of cursor
	int16_t saveY = tft.getCursorY();

	eraseNoSignal();
	noSignalOn = true;
	tft.setTextSize(noSigTextSize);
	tft.setCursor(NOSIG_X, NOSIG_Y);		// x,y
	if(ledToggle){
		tft.setTextColor(ILI9341_YELLOW);
	}else{
		tft.setTextColor(ILI9341_RED);
	}
	tft.println(noSignalString);
		// restore cursor.
	tft.setCursor(saveX, saveY);

	return;
}

// display DX number on GFX LCD
void displayDx(int16_t dx){
	int16_t saveX = tft.getCursorX();		// save location of cursor
	int16_t saveY = tft.getCursorY();

	// Need to clear any existing number
	tft.setCursor(dxX,dxY);
	tft.setTextSize(6);
	eraseString((char*)"88888");
	tft.setCursor(dxX,dxY);
	// display new number
	tft.setTextColor(ILI9341_GREEN);
	if(dx >= 0 && dx < OUT_OF_RANGE_DX){
		tft.print(dx);
		tft.setTextSize(2);
		tft.setCursor(tft.getCursorX(),dxY+24);
		tft.print("mm");
	}
	eraseNoSignal();
		// restore cursor.
	tft.setCursor(saveX, saveY);
}

void displayDeltaTime(uint32_t time_ticks){
	int16_t saveX = tft.getCursorX();		// save location of cursor
	int16_t saveY = tft.getCursorY();
	tft.setCursor(deltaX,deltaY);
	tft.setTextColor(ILI9341_YELLOW);
	tft.setTextSize(2);
	#if(UNITS_SCALE == 1)
	eraseString((char*)"88888888 nSec");
	if(time_ticks<(OUT_OF_RANGE_TIME)){
		tft.print(time_ticks);
		tft.print(" nSec");
	}
	#else
	eraseString((char*)"88888 uSec");
	if(time_ticks<(OUT_OF_RANGE_TIME)){
		tft.print(time_ticks);
		tft.print(" uSec");
	}
	#endif
	tft.setCursor(saveX, saveY);
}

// calculate sweep degree from time of sweep
float getDegreesFromTime(uint32_t time_ticks)
{
	float degrees = 0.0;
	if(time_ticks<(OUT_OF_RANGE_TIME))
		degrees = f_calib_DegreePerTimeUnit * (float)time_ticks;
	return degrees;
}

// calculate degree beam sweep from delta time, store in global f_s1s2_degrees
// Time units can be uSec or nSec based on global macro UNITS_SCALE
void setDegreesFromTime(uint32_t time_ticks)
{
	if(time_ticks<(OUT_OF_RANGE_TIME))
		f_s1s2_degrees = f_calib_DegreePerTimeUnit * (float)time_ticks;
}

// calculate degrees +/- from lighthouse center line.
void setDegreesHorizFromTime(int32_t time_ticks)
{
	f_horiz_degrees = f_calib_DegreePerTimeUnit * (float)time_ticks;
}
// calculate degrees +/- from lighthouse center line.
void setDegreesVertFromTime(int32_t time_ticks)
{
	f_vert_degrees = f_calib_DegreePerTimeUnit * (float)time_ticks;
}

// display an angle at requested location.
void displayAngleAtXY(int16_t angleX, int16_t angleY, float f_degrees)
{
	int16_t saveX = tft.getCursorX();		// save location of cursor
	int16_t saveY = tft.getCursorY();
	tft.setCursor(angleX,angleY);
	tft.setTextColor(ILI9341_YELLOW);
	tft.setTextSize(2);
	eraseString((char*)"888.88 deg");
	tft.print(f_degrees);
	tft.print(" deg");
	tft.setCursor(saveX, saveY);
}

// display the sweep angle f_s1s2_degrees (call setDegreesFromTime() first)
void displaySweepAngle()
{
	displayAngleAtXY(sweepX,sweepY,f_s1s2_degrees);
}
// display the horizontal angle f_horiz_degrees (call setDegreesHorizFromTime() first)
void displayHorizAngle()
{
	displayAngleAtXY(sweepX,sweepY+16,f_horiz_degrees);
}
// display the vertical angle f_vert_degrees (call setDegreesVertFromTime() first)
void displayVertAngle()
{
	displayAngleAtXY(sweepX,sweepY+32,f_vert_degrees);
}

// display xyz location number
void displayLocAtXY(int16_t locX, int16_t locY, float locNumber)
{
	int16_t saveX = tft.getCursorX();		// save location of cursor
	int16_t saveY = tft.getCursorY();
	tft.setCursor(locX,locY);
	tft.setTextColor(ILI9341_GREEN);
	tft.setTextSize(XYZ_NUMBER_SIZE);
	eraseString((char*)"00000");
	tft.print((int)locNumber);				// display as integer for now.
	tft.setCursor(saveX, saveY);
}
// display X location
void displayXloc()
{
	displayLocAtXY(xX,xY,f_lh_x);
}
// display Y location
void displayYloc()
{
	displayLocAtXY(yX,yY,f_lh_y);
}
// display Z location
void displayZloc()
{
	displayLocAtXY(zX,zY,f_lh_z);
}


int displayField=0;		// which time field to display. 0=horiz.  1=vert.

// dump the raw pulse timing measurements via Serial.
void dumpMeasure(){
	bool go=false;
	if(get_ir1_state() == IR_idle){
		Serial.print("IR1 Times: ");
		for(int i=0; i<16;i++){
			Serial.print(ir1_FieldTimes[i]);
			Serial.print(" ");
		}
		go = true;
		Serial.println(" ");
	}
	if(get_ir2_state() == IR_idle){
		Serial.print("IR2 Times: ");
		for(int i=0; i<16;i++){
			Serial.print(ir2_FieldTimes[i]);
			Serial.print(" ");
		}
		go = true;
		Serial.println(" ");
	}
	if( go ){
		delay(400);
		lastMode=digitalRead(MODE_PIN);
		toggleLED();
		timerClear();
		timerMsec(NOSIG_TIMEOUT);		// ten second timer start (1 mSec less for above Serial IO time)
		ir_go(lastMode);
	}
}

// MATH: Calculate radius distance from lighthouse
// using ISR's timing data from S1 and S2
// update global var dx.
//
// Using Linear approximation method:
//		(assume sin(a) of angles less than 30 degrees is linear == a)
//	Distance (Dx) of S1 S2 sensor plane from lighthouse will then be:
//		Dx = ( calib_S1S2Time / S1S2_deltaTime ) * calib_Dx  (270 mm)
uint32_t calculateRadius()
{
	//	Dx = ( calib_S1S2Time / S1S2_deltaTime ) * calib_Dx  (255 mm)
	S1S2_deltaTime = abs((long long)ir2_FieldTimes[T_HORIZONTAL] - (long long)ir1_FieldTimes[T_HORIZONTAL]);
	if(S1S2_deltaTime!=0){
		dx = ((((calib_S1S2Time*1000) / S1S2_deltaTime) * calib_Dx)+500)/1000;
	}else{
		// time measurement resolution not able to resolve distance
		dx = OUT_OF_RANGE_DX;	// set large distance.
	}
	return dx;
}

// MATH: Calculate X location relative to lighthouse
// using radius and beam angle Φ in degrees.
// Where Φ is the angle left or right of the center of the lighthouse.
// Updates the global var: f_lh_x
// return result is in mm +/- from lighthouse center line.
float calculateX(float radius, float angle)
{
	// sin Φ = X/Radius
	float x = radius * sin((angle*3.1415/180.0));
	f_lh_x = x;
	return f_lh_x;
}
// MATH: Calculate Y location relative to lighthouse
// using radius and beam angle Θ in degrees.
// Where Θ is the angle above or below the center of the lighthouse
// Updates the global var: f_lh_y
// return result is in mm +/- from lighthouse center line.
float calculateY(float radius, float angle)
{
	// sin Θ = Y/Radius
	float y = radius * sin((angle*3.1415/180.0));
	f_lh_y = y;
	return f_lh_y;
}
// MATH: Calculate Z location relative to lighthouse
// using Distance formula since we have radius, x and y
// Dx = SQRT( X^2 + Y^2 + Z^2 )
//
// Updates the global var: f_lh_z
// return result is in mm +/- from lighthouse center line.
float calculateZ(float radius, float x, float y)
{
	float z2 = radius * radius - x*x - y*y;
	if( z2 > 0.5 )
		f_lh_z = sqrt(z2);
	else
		f_lh_z = 0.0;
	return f_lh_z;
}

//-----------------------------------------------------------
// Calculate and display the distance from the lighthouse
// use simple linear approximation (no trig math)
void showDistance()
{
	if( get_ir1_state() == IR_idle && get_ir2_state() == IR_idle){
		// have data to measure distance Z from lighthouse.
		Serial.print("IR idle ");
		displayField = digitalRead(AXIS_PIN);
		displayField++;
		Serial.print("  AXIS: ");
		Serial.println(displayField);

		// use two sensor to display radial DX from light house
		calculateRadius();
		// calculate and set degree beam sweep
		setDegreesFromTime(S1S2_deltaTime);

		// X and Y calculations
		if(dx < (OUT_OF_RANGE_DX))
		{
			// X
			// assume S1 is always hit first by beam.
			arc_time_horiz = ir1_FieldTimes[T_HORIZONTAL] + S1S2_deltaTime/2;
			Horiz_deltaTime = arc_time_horiz - ARC_TIME_CENTER;
			setDegreesHorizFromTime(Horiz_deltaTime);
			calculateX((float)dx, f_horiz_degrees);
			// Y
			// assume S1 is always hit first by beam.
			arc_time_vert = ir1_FieldTimes[T_VERTICAL];
			Vert_deltaTime = arc_time_vert - ARC_TIME_CENTER;
			setDegreesVertFromTime(Vert_deltaTime);
			calculateY((float)dx, f_vert_degrees);
			// Z
			calculateZ((float)dx, f_lh_x, f_lh_y);
		}
		// ---------------------
		// Display Update
		// ---------------------
#ifdef MC145000_H
		LCDprint(dx);
#endif
#ifdef NEWLCD
		displayDx((int16_t)dx);
		displayDeltaTime(S1S2_deltaTime);
		displaySweepAngle();
		displayHorizAngle();
		displayVertAngle();
		displayXloc();
		displayYloc();
		displayZloc();
#endif
		// ---------------------
		// Serial Port Report
		// ---------------------
		serial_paramReport();

		delay(LED_BLINK_GOODTIME);
		lastMode=digitalRead(MODE_PIN);
		toggleLED();
		timerClear();
		timerMsec(NOSIG_TIMEOUT);		// no signal timer start
		ir_go(lastMode);
	}
}

// ---------------------
// Serial Port Report
// ---------------------
void serial_paramReport()
{
	#if NANOTICKS
	Serial << "nSec: ";
	#else
	Serial << "uSec: ";
	#endif
	Serial << fastTicks() << '\n';
	Serial << "IR1 Times:  ";
	for(int i=0; i<16;i++){
		Serial << ir1_FieldTimes[i] << ' ';
	}
	Serial.println(" ");
	Serial << "IR2 Times:  ";
	for(int i=0; i<16;i++){
		Serial << ir2_FieldTimes[i] << ' ';
	}
	Serial.println(" ");
	Serial << "Sync Count: " << ir1_syncCount << '\n';
	Serial << " S1S2_deltaTime: " << S1S2_deltaTime << '\n';
	Serial << "S1S2 Beam Angle: " << f_s1s2_degrees << " deg" << '\n';
	Serial << " Horiz arc time: " << arc_time_horiz << '\n';
	Serial << "    Horiz delta: " << Horiz_deltaTime << '\n';
	Serial << "    Horiz Angle: " << f_horiz_degrees << " deg" << '\n';
	Serial << "  Vert arc time: " << arc_time_vert << '\n';
	Serial << "     Vert delta: " << Vert_deltaTime << '\n';
	Serial << "     Vert Angle: " << f_vert_degrees << " deg" << '\n';
	Serial << "Radius: " << dx << '\n';
	Serial << "X: " << f_lh_x << '\n';
	Serial << "Y: " << f_lh_y << '\n';
	Serial << "Z: " << f_lh_z << '\n';

	Serial << '\n';
}
// ---------------------------------
// ******* Serial port menu  *******
// ---------------------------------
const char menuHelp[]={"\nCommands:\n"
	"d - display update force\n"
	"i - information report\n"
	"s - wait for Sync in raw measurement mode (toggle)\n"
	"p - Pause \"No IR Signal\" reporting (toggle)\n"
	"r - Reset system \n"
	"v - version number & build timestamp\n"
	};
bool bPaused=true;

// check for a menu command on serial port
void commandMenu()
{
	if (Serial.available() > 0) {
		// Check for commands.
		char inChar = (char)Serial.read();
		if(inChar > 0x19){
			Serial.print(inChar);// echo input
			Serial.print(" ");
			switch( inChar ){
				case 'd':
				Serial << "Display Update" << '\n';
				// set ISR's to idle state to force display update
				ir_stopDx();
				break;

			case 'i':
				Serial.println("Information: ");
				serial_paramReport();
				break;

			case 's':	// toggle measure mode to wait for sync or not.
				bWaitSyncMeasure = bWaitSyncMeasure ? false : true;
				Serial.print("Measure mode WaitSync: ");
				Serial.print(bWaitSyncMeasure);
				break;

			case 'p':	// toggle the no signal messages
				bPaused = bPaused ? false:true;
				Serial.print("Paused: ");
				Serial.print(bPaused);
				break;

			case 'r':	// reset the MCU
				Serial.println("  ** RESET requested ** ");
				delay(2000);		// wait for serial output to finish
				digitalWrite(RESET_PIN, LOW);	// todo: need this anymore??  This hangs the Fubarino in constant reset unless external pull up added.
				resetFunc(); //call chipKit reset  -- if RESET_PIN is not connected
				break;

			case 'v':
				Serial.print(VERSION);
				Serial.print(BUILDTIME);
				break;

			case '#':	// starts a comment line for terminal log
				Serial << Serial.readStringUntil('\n');
				break;	// echo line and ignore

			case '?':
			default:// unknown cmd, give help menu
				Serial.print(menuHelp);
				break;
			}
			Serial.println(" ");
		}
	}
}



// ==========================================================================
// ==========================================================================
//		The loop
// ==========================================================================
// ==========================================================================
void loop()
{
    digitalWrite(OS_SYNC, digitalRead(OS_SYNC)^1);  // for oscilloscope triggering

	int sim_done;
	sim_done=simsrc_generator(SIMSRC_PIN1,SIMSRC_PIN2);// generate simulated pattern for testing.
	if( sim_done )
	{
		if( lastMode ){				//  mode:  0 == measure,  1 == h/v timing or distance
			showDistance();			// display distance measurement if data is ready.
		}else{
			dumpMeasure();			// dump any raw measurement that are ready.
		}
	}
	if(lastMode != digitalRead(MODE_PIN)){
		// mode changed, restart ISR state machines
		delay(300);					// delay for a crude debounce
		lastMode=digitalRead(MODE_PIN);
		Serial << "Change Measurement Mode: "<< lastMode << modeString(lastMode) << '\n';
		toggleLED();
		timerClear();
		timerMsec(NOSIG_TIMEOUT);	// timeout timer start
		ir_go(lastMode);
	}
	// Check no signal timer
	if( timerExpired()){
		if(!bPaused ){
			if(ledToggle){
				// space every other time to make message update obvious when lots of messages
				Serial << " ";
			}
			Serial << " No IR Signal: ";
			if(!ledToggle){
				// space every other time to make message update obvious when lots of messages
				Serial << " ";
			}
			Serial << "IR1st: " << irStateString(get_ir1_state());
			Serial << " c:" << ir1_count << "   ";
			Serial << "IR2st: " << irStateString(get_ir2_state());
			Serial << " c:" << ir2_count;
			Serial << '\n';
		}
#ifdef NEWLCD
		eraseNoSignal();
		displayNoSignal();
#endif
		toggleLED();
		timerClear();
		timerMsec(NOSIG_TIMEOUT);	// LOS timeout timer start
		ir_go(lastMode);			// restart the state machines.
	}
	commandMenu();					// process any serial port commands
}
/* ********************************************************************
Notes:
For 1 mm resolution at 3 m using 33 mm spacing between S1 & S2, a system timer tick rate of 19 nSec or faster is required.

Was using v1.3.1 of chipKit lib to build up to v1.04  (Chipkit 1.4.0 update seems OK on v1.05)
Building as board: Fubarino SD (1.5)
	Use macro for pic32 only code: ARDUINO_ARCH_PIC32

On Linux the fubarinoSD board packages are loaded to:
	~/.arduino15/packages/chipKIT/

For chipKit to work on Linux, need to install 32 bit support:
	 sudo apt-get install libc6-i386

On windows the board packages are loaded to:
	C:\Users\xxxxxxx\AppData\Local\Arduino15\packages\chipKIT

ChipKit arduino libs: http://chipkit.net/wiki/index.php?title=ChipKIT_core#2.29_Manual_install_by_copying_ZIP_file

On Linux the STM32 board packages are loaded to:
	~/.arduino15/packages/STMicroelectronics/

Triangle calculator:
http://www.calculator.net/triangle-calculator.html?vc=&vx=6.6&vy=&va=90&vz=100.0&vb=&angleunits=d&x=55&y=9
Trig math:
https://mathbits.com/MathBits/TISection/Trig/AreaTrigTri.htm


LICENSE:
---------

Copyright 2017 Eastan Giebler, Lucas Giebler, Mark Giebler

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.


************************************************************************** */
