# A device to measure 3D position relative to an HTC Vive VR lighthouse

[![Release](https://gitlab.com/mark-giebler/IR-Vive/-/badges/release.svg)](https://gitlab.com/mark-giebler/IR-Vive/-/releases)
[![Pipeline Status](https://gitlab.com/mark-giebler/IR-Vive/badges/master/pipeline.svg)](https://gitlab.com/mark-giebler/IR-Vive/-/pipelines)

 Eastan Giebler, Lucas Giebler, Mark Giebler

This is a project my son's & I built to show how the HTC Vive Virtual Reality lighthouses work.

Using two IR sensors facing an HTC Vive lighthouse, this device can determine
its 3D location (X, Y, Z) relative to an HTC Vive lighthouse with 1 mm resolution.

See comments in the source code for details on how the software works and the math involved.

## Table of Contents

[[_TOC_]]


## Hardware Used

* FubarinoSD(1.5) or Nucleo STM32G474RE or Nucleo STM32L476RG
* TFT LCD: 2.4 or [2.8 inch with ILI9341 controller](https://www.amazon.com/dp/B073R7BH1B/ref=cm_sw_em_r_mt_dp_CEF76J1HY7EE6V6CTADX?_encoding=UTF8&psc=1)
* Two [Fairchild QSE156](https://www.digikey.com/en/products/detail/on-semiconductor/QSE156/187473) IR sensors
* An HTC Vive VR Lighthouse

## Arduino Libraries

The following libraries are required:
- Adafruit_GFX_Library
- Adafruit_BusIO
- Adafruit_ILI9341

> See the source code for information on modifications that were required
to get the Adafruit libraries to build correctly with a PIC32 device (Fubarino SD).
If you are not using a PIC32 board, the modifications are not required.


## Wiring

The [Fritzing](https://fritzing.org/) wiring diagrams are shown below.

The distance between the center pins of IR sensors S1 and S2 must be 6.6 cm (2.6 inches).

### Fuborino SD Board

![Image: FubarinoSD](./Fritzing/IR_Vive_fubarino_bb.png)

### Nucleo STM32G474RE Board

> Note: wiring for the Nucleo STM32L476RG board is identical.

![Image: Nucleo](./Fritzing/IR_Vive_nucleo_bb.png)

## Schematics

The schematics for the board connections to the LCD and IR sensors.

### Fuborino SD Board

![Image: FubarinoSD](./Fritzing/IR_Vive_fubarino_schem.png)


### Nucleo STM32G474RE Board

> Note: wiring for the Nucleo STM32L476RG board is identical.

![Image: Nucleo](./Fritzing/IR_Vive_nucleo_schem.png)

## Local and Cloud CI Build

This project has CI build scripts that allow the project to be built from the local comand line
as well as in a Gitlab CI pipeline.  The source code has **CI Meta Tags** that specify the target
board hardware as well as the required Arduino libraries.

See the [CI Meta tag project](https://gitlab.com/mark-giebler/test__arduino_ci#test-arduino-ci-build-in-gitlab-and-locally)
for more information on how the **CI Meta Tags** build works and how to set it up.


## Other

For Board packages and libraries required, see comments in the source code.

To get programming Nucleo boards via SWD to work on Linux, had to make links to ST's bin utils:
```
sudo ln -s /usr/local/STMicroelectronics/STM32Cube/STM32CubeProgrammer/bin/STM32_Programmer.sh /usr/local/bin/STM32_Programmer.sh
sudo ln -s /usr/local/STMicroelectronics/STM32Cube/STM32CubeProgrammer/bin/STM32_Programmer_CLI /usr/local/bin/STM32_Programmer_CLI
```

> mc145000.cpp and mc145000.h  are no longer used in the version committed to gitlab.

# References

The automated build and release CI was based on the project from [here.](https://gitlab.com/mark-giebler/test__arduino_ci/)

# License Stuff

 Copyright 2017 Eastan Giebler, Lucas Giebler, Mark Giebler

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

