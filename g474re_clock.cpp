/*
 *******************************************************************************
 * Copyright (c) 2020-2021, STMicroelectronics
 * All rights reserved.
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 
 Modify system clock frequency to get an even number for sysTimer tick rate in nano seconds. 
 Ideal: 125 MHz and 8.0 nSec.
 Some Options:
 On STM32G474:
 Using HSI for PLL ref:
	 124 MHz (8.035 nSec) with PLLdiv at 4, 62, 2
	 125.333333 MHz (7.979797 nSec) prePLLdiv 6, PLLdiv 94, 2
	 142.666667 MHz (7.009 nSec)  prPLLdiv 6, PLLdiv 107, postPLLdiv 2,4,2
	 83.2 (12.02 nSec)	prPLLdiv 5, PLLdiv 103, postPLLdiv 2,4,2

 Using external 24MHz xtal for PLL ref.:
	124.5 MHz (8.032 nSec) 8, 83, 2
	125.142857 MHz (7.991 nSec) 7, 73, 2	*** Use this option for G474

 */
#if defined(ARDUINO_NUCLEO_G474RE)
#include "pins_arduino.h"

// uncomment to enable sysClk out on pin D7
#define ENABLE_MCO

#define MCO1_CLK_ENABLE()   __HAL_RCC_GPIOA_CLK_ENABLE()
#define MCO1_GPIO_PORT        GPIOA
#define MCO1_PIN              GPIO_PIN_8

// Define PLL config parameters for various frequencies

//  125.142857 MHz (7.991 nSec) 7, 73, 2.  24MHz xtal PLL Ref.
#define MCLK_125142857_HZ \
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI48|RCC_OSCILLATORTYPE_HSE; \
	RCC_OscInitStruct.HSEState = RCC_HSE_ON; \
	RCC_OscInitStruct.HSI48State = RCC_HSI48_ON; \
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON; \
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE; \
	RCC_OscInitStruct.PLL.PLLM = RCC_PLLM_DIV7; \
	RCC_OscInitStruct.PLL.PLLN = 73; \
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2; \
	RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV6; \
	RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;

// 124.5 MHz (8.032 nSec) 8, 83, 2.  24MHz xtal PLL Ref.
#define MCLK_124500000_HZ \
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI48|RCC_OSCILLATORTYPE_HSE; \
	RCC_OscInitStruct.HSEState = RCC_HSE_ON; \
	RCC_OscInitStruct.HSI48State = RCC_HSI48_ON; \
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON; \
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE; \
	RCC_OscInitStruct.PLL.PLLM = RCC_PLLM_DIV8; \
	RCC_OscInitStruct.PLL.PLLN = 83; \
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2; \
	RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV6; \
	RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;

// 165.6 MHz (6.039 nSec) 5, 69, 2.  24MHz xtal PLL Ref.
#define MCLK_165600000_HZ \
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI48|RCC_OSCILLATORTYPE_HSE; \
	RCC_OscInitStruct.HSEState = RCC_HSE_ON; \
	RCC_OscInitStruct.HSI48State = RCC_HSI48_ON; \
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON; \
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE; \
	RCC_OscInitStruct.PLL.PLLM = RCC_PLLM_DIV5; \
	RCC_OscInitStruct.PLL.PLLN = 69; \
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2; \
	RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV6; \
	RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;

// 125.333333 MHz (7.979797 nSec) prePLLdiv 6, PLLdiv 94, PLLdivR 2.   HSI PLL ref..
#define MCLK_125333333_HZ \
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI48|RCC_OSCILLATORTYPE_HSI; \
	RCC_OscInitStruct.HSEState = RCC_HSE_ON; \
	RCC_OscInitStruct.HSI48State = RCC_HSI48_ON; \
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON; \
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI; \
	RCC_OscInitStruct.PLL.PLLM = RCC_PLLM_DIV6; \
	RCC_OscInitStruct.PLL.PLLN = 94; \
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2; \
	RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV6; \
	RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;


// select which PLL config we want. Manually coordinate nSec value in STM32_CLOCK_NANOS in g474re_clock.h
#define MyCLK_PLLconfig() MCLK_125142857_HZ

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/**
  * @brief  System Clock Configuration
  * Replace the stm32 default SystemClock_Config()
  * Set up system clock to MyCLK_PLLconfig() settings.
  * @param  None
  * @retval None
  */
void SystemClock_Config(void)
{
	RCC_OscInitTypeDef RCC_OscInitStruct = {0};
	RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
	RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

	/** Configure the main internal regulator output voltage
	*/
	HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1_BOOST);
	/** Initializes the RCC Oscillators according to the specified parameters
	* in the RCC_OscInitTypeDef structure.
	*/
	MyCLK_PLLconfig();
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
		Error_Handler();
	}
	/** Initializes the CPU, AHB and APB buses clocks
	*/
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
								| RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK) {
		Error_Handler();
	}
	/** Initializes the peripherals clocks
	*/
	PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USB;
	PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_HSI48;
	if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK) {
		Error_Handler();
	}

	// update core freq. global variable.
	SystemCoreClockUpdate();
  
#ifdef ENABLE_MCO
	HAL_RCC_MCOConfig(RCC_MCO1, RCC_MCO1SOURCE_SYSCLK, RCC_MCODIV_1);
#endif
}

#ifdef __cplusplus
} // extern "C"
#endif

#endif /* ARDUINO_NUCLEO_G474RE */
