#ifndef IR_VIVE_H
#define IR_VIVE_H

// uncomment NANOTICKS to get 10's of nSec resolution on sensor timing - required for 1mm location resolution. Else uses uSec res.
#define NANOTICKS	1

#ifdef NANOTICKS
#define UNITS_SCALE 1			// no scale.  constants in nSec resolution
#define OUT_OF_RANGE_TIME	(9999000)// time value for out of range. nSec units.
#else
#define UNITS_SCALE 1/1000		// scale down to uSec resolution
#define OUT_OF_RANGE_TIME	(9999)	// time value for out of range. uSec units.
#endif

// Define IR pulse timings in nSec resolution

// HTC Lighthouse's Sync frame time 8333.333 uSec (120 Hz)
#define PULSE_RATE_SYNC		(8333333ul*UNITS_SCALE)	// in nanoseconds units
#define PULSE_SYNC_WIDTH	(  70000ul*UNITS_SCALE)
#define PULSE_SWEEP_WIDTH	(  33000ul*UNITS_SCALE)
#define PRS_TOLERANCE		( 185000ul*UNITS_SCALE)	// tolerance on SYNC pulse period.
#define PR_SYNC_LOWER		(PULSE_RATE_SYNC-PRS_TOLERANCE)
#define PR_SYNC_UPPER		(PULSE_RATE_SYNC+PRS_TOLERANCE)
#define MAXPULSE			(PULSE_RATE_SYNC+3*PRS_TOLERANCE)

// define center time for horizontal and vertical sweep pulses
#define ARC_TIME_CENTER		(PULSE_RATE_SYNC/2ul)


// define LED blink rate when IR signal good. LCD update time gets added on. Thus blink rate is slower than this.
#define LED_BLINK_GOODTIME	(80)		// mSec

#define NOSIG_TIMEOUT 		(3999)		// no IR signal timeout rate. mSec.

#define OUT_OF_RANGE_DX		(9999)		// out of range distance.

#endif		// IR_VIVE_H
